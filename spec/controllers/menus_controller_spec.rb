# frozen_string_literal: true

require 'rails_helper'

describe MenusController, type: :controller do
  context 'when menus exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_menus, menus_count: 1) }

    # Get list of menus's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, params: { restaurant_id: 1 }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET menu object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: user.menus.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # PUT menu object
    describe 'PUT #update' do
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: { id: user.menus.first.id, price: 2.10 }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.menus.first.price).to eql(2.10)
      end
    end
    # DELETE menu object
    describe 'DELETE #delete' do
      it 'responds successfully with an HTTP 200 status code' do
        delete :destroy, params: { id: user.menus.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.menus.count).to eql(0)
      end
    end
  end

  context 'when menus does not exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_restaurants, restaurants_count: 1) }
    # Create menu object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: { restaurant_id: user.restaurants.first.id }.merge(FactoryBot.attributes_for(:menu))
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.menus.count).to eql(1)
      end
    end
  end

  context 'when request is not authorized' do
    let(:user) { create(:user_with_menus, menus_count: 1) }
    # GET menu object
    describe 'GET #show' do
      it 'responds with HTTP 401 status code' do
        get :show, params: { id: user.menus.first.id }
        expect(response).to have_http_status(401)
      end
    end
  end
end
