# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AuthenticationController, type: :controller do
  let(:user) { create(:user) }
  let(:header) { { 'Authorization' => JsonWebToken.encode(user_id: user.id) } }

  before(:each) do
    @token = JsonWebToken.encode(user_id: user.id)
  end

  describe '#create' do
    it 'responds with a JWT' do
      expect(@token).to be_kind_of(String)
      segments = @token.split('.')
      expect(segments.size).to eql(3)
    end
  end
end
