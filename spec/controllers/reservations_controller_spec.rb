# frozen_string_literal: true

require 'rails_helper'

describe ReservationsController, type: :controller do
  context 'when reservations exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_reservations, reservations_count: 1) }
    let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }

    # GET list of reservation's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, params: { restaurant_id: user.restaurants.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET reservation object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: user.reservations.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # PUT reservation object
    describe 'PUT #update' do
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: {
          id: user.reservations.first.id,
          guest_number: 2,
          table_id: table.id,
          extend_time: 30
        }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.reservations.first.guest_number).to eql(2)
      end
    end
    # DELETE reservation object
    describe 'DELETE #delete' do
      it 'responds successfully with an HTTP 200 status code' do
        delete :destroy, params: { id: user.reservations.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.reservations.count).to eql(0)
      end
    end
  end

  context 'when reservations does not exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_restaurants, restaurants_count: 1) }
    let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }
    # Create reservation object
    describe 'POST #create' do
      it 'responds successfully with HTTP 200 status code or HTTP 400 when reservations are closed' do
        post :create, params: { restaurant_id: user.restaurants.first.id, table_id: table.id }.merge(FactoryBot.attributes_for(:reservation))
        if (Settings.global_time_window.end_hour..Settings.global_time_window.start_hour).to_a.include? Time.now.getlocal.hour
          expect(response).to be_successful
          expect(response).to have_http_status(200)
          expect(user.reservations.count).to eql(1)
        else
          expect(response).to have_http_status(400)
          expect(user.reservations.count).to eql(0)
        end
      end
    end

    describe 'GET #show' do
      # GET reservation object
      it 'responds with HTTP 404 status code' do
        get :show, params: { id: 1 }
        expect(response).to have_http_status(404)
      end
    end
  end

  context 'when request is not authorized' do
    let(:user) { create(:user_with_reservations, reservations_count: 1) }
    # GET reservation object
    describe 'GET #show' do
      it 'responds with HTTP 401 status code' do
        get :show, params: { id: user.reservations.first.id }
        expect(response).to have_http_status(401)
      end
    end
  end
end
