# frozen_string_literal: true

require 'rails_helper'

describe RestaurantsController, type: :controller do
  context 'when restaurants exists' do
    let(:user) { create(:user_with_restaurants, restaurants_count: 5, id_role: 1) }
    # GET list of restaurant's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code for unauthorized request' do
        get :index
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET restaurant object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code for unauthorized request' do
        get :show, params: { id: user.restaurants.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # PUT restaurant object
    describe 'PUT #update' do
      before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: { id: user.restaurants.first.id, name: 'changed_name' }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # DELETE restaurant object
    describe 'DELETE #delete' do
      before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
      it 'responds successfully with an HTTP 200 status code' do
        delete :destroy, params: { id: user.restaurants.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.restaurants.count).to eql(4)
      end
    end
  end

  context 'when restaurants does not exists' do
    let(:user) { create(:user, id_role: 1) }
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    # CREATE restaurant object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: FactoryBot.attributes_for(:restaurant)
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.restaurants.count).to eql(1)
      end
    end
  end
end
