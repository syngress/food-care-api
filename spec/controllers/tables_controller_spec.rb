# frozen_string_literal: true

require 'rails_helper'

describe TablesController, type: :controller do
  context 'when tables exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
    let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }

    # GET list of table's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, params: { restaurant_id: user.restaurants.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET table object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: table.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body).count).to eq(1)
      end
    end
    # PUT table object
    describe 'PUT #update' do
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: { id: table.id, name: 'TableABC' }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['name']).to eq('TableABC')
      end
    end
    # DELETE table object
    describe 'DELETE #delete' do
      it 'responds successfully with an HTTP 200 status code' do
        table
        expect(user.restaurants.last.tables.count).to eq(1)
        delete :destroy, params: { id: table.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.restaurants.last.tables.last).to be(nil)
      end
    end

    context 'when tables does not exists' do
      let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }

      before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
      # Create table object
      describe 'POST #create' do
        it 'responds successfully with an HTTP 200 status code' do
          expect(user.restaurants.last.tables.count).to eq(0)
          post :create, params: FactoryBot.attributes_for(:table, restaurant_id: user.restaurants.last.id)
          expect(response).to be_successful
          expect(response).to have_http_status(200)
          expect(user.restaurants.count).to eq(1)
        end
      end
    end
  end
  context 'when request is not authorized' do
    let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
    let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }

    # GET table object
    describe 'GET #show' do
      it 'responds with HTTP 200 status code for unauthorized request' do
        get :show, params: { id: table.id }
        expect(response).to have_http_status(200)
      end
    end
  end
end
