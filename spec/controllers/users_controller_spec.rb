# frozen_string_literal: true

require 'rails_helper'

describe UsersController, type: :controller do
  context 'when user does not exists' do
    # Create user object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: FactoryBot.attributes_for(:user)
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(User.all.count).to eql(1)
      end
    end
  end

  context 'when user exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }

    # Get list of user's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(User.all.count).to eql(1)
      end
    end
    # GET user object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: user.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(user.id_role).to eql(1)
      end
    end
    # PUT user object
    describe 'PUT #update' do
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: { id: user.id, name: 'SomeNewName' }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # DELETE user object
    describe 'DELETE #delete' do
      it 'responds successfully with an HTTP 200 status code' do
        delete :destroy, params: { id: user.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(User.all.count).to eql(0)
      end
    end
  end

  context 'when request is not authorized' do
    let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
    # GET user object
    describe 'GET #show' do
      it 'responds with HTTP 401 status code' do
        get :show, params: { id: 1 }
        expect(response).to have_http_status(401)
      end
    end
  end
end
