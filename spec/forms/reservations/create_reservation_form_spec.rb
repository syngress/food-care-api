# frozen_string_literal: true

require 'rails_helper'

describe Reservations::CreateReservationForm, type: :model do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }

  subject do
    described_class.new(
      attributes_for(:reservation)
    )
  end

  context 'when reservation form is invalid' do
    it 'throw validation error for form without table_id' do
      subject.objects.merge!(restaurant: user.restaurants.first)
      expect(subject.valid?).to eq(false)
    end
  end

  context 'when reservation form is valid' do
    let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }

    it 'successfully perform form validation with table_id' do
      table
      subject.objects.merge!(restaurant: user.restaurants.first)
      if (Settings.global_time_window.end_hour..Settings.global_time_window.start_hour).to_a.include? Time.now.getlocal.hour
        expect(subject.valid?).to eq(true)
      else
        expect(subject.valid?).to eq(false)
      end
    end
  end
end
