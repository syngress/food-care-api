# frozen_string_literal: true

require 'rails_helper'

describe Restaurants::UpdateRestaurantForm, type: :model do
  too_long_string = 'a' * 300
  subject do
    described_class.new(
      attributes_for(:restaurant)
    )
  end

  context 'when restaurant form is valid' do
    it 'perform form validation' do
      expect(subject.valid?).to eq(true)
    end

    it 'validate presence of form attributes' do
      should_not validate_presence_of(:name)
      should_not validate_presence_of(:address)
      should_not validate_presence_of(:description)
      should_not validate_presence_of(:guest_capacity)
      should_not validate_presence_of(:booking_capacity)
    end
  end

  context 'when form attributes are set to nil' do
    it 'validate nil attributes' do
      should allow_value(nil).for(:name)
      should allow_value(nil).for(:address)
      should allow_value(nil).for(:description)
      should_not allow_value(nil).for(:guest_capacity)
      should_not allow_value(nil).for(:booking_capacity)
    end
  end

  context 'when restaurant form is invalid' do
    it 'validate :guest_capacity attribute type' do
      should_not allow_value('some_wrong_attr_type').for(:guest_capacity)
      expect(subject.errors.messages[:guest_capacity].first).to eq('The maximum number of guests has not been specified')
      expect(subject.errors).to be_an_instance_of(ActiveModel::Errors)
      expect(subject.valid?).to eq(false)
    end

    it 'validate :booking_capacity attribute type' do
      should_not allow_value('some_wrong_attr_type').for(:booking_capacity)
      expect(subject.errors.messages[:booking_capacity].first).to eq('Not a number')
      expect(subject.errors).to be_an_instance_of(ActiveModel::Errors)
      expect(subject.valid?).to eq(false)
    end

    it 'validate length of the :name attribute' do
      should_not allow_value(too_long_string).for(:name)
      expect(subject.errors.messages[:name].first).to eq('Too long')
      expect(subject.errors).to be_an_instance_of(ActiveModel::Errors)
      expect(subject.valid?).to eq(false)
    end

    it 'validate length of the :address attribute' do
      should_not allow_value(too_long_string).for(:address)
      expect(subject.errors.messages[:address].first).to eq('Too long')
      expect(subject.errors).to be_an_instance_of(ActiveModel::Errors)
      expect(subject.valid?).to eq(false)
    end

    it 'validate length of the :description attribute' do
      should_not allow_value(too_long_string).for(:description)
      expect(subject.errors.messages[:description].first).to eq('Too long')
      expect(subject.errors).to be_an_instance_of(ActiveModel::Errors)
      expect(subject.valid?).to eq(false)
    end
  end
end
