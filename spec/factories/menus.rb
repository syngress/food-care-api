# frozen_string_literal: true

FactoryBot.define do
  factory :menu, class: Menu do
    category { Faker::Verb.base }
    name { Faker::Verb.base }
    description { Faker::Verb.base }
    price { 10.30 }
    currency { 'PLN' }

    association :restaurant, factory: :restaurant
  end

  factory :test_menu_model, class: Menu do
    category { 'SomeCategory' }
    name { 'SomeName' }
    description { 'SomeDescription' }
    price { 13.11 }
    currency { 'PLN' }
  end
end
