# frozen_string_literal: true

FactoryBot.define do
  factory :restaurant, class: Restaurant do
    name { Faker::Name.name }
    address { Faker::Address.full_address }
    description { Faker::Marketing.buzzwords }
    guest_capacity { Faker::Number.between(1, 10) }
    booking_capacity { Faker::Number.between(1, 10) }

    association :user, factory: :user

    # has_many
    trait :with_reservations do
      transient { reservations_count { 1 } }
      after(:create) do |restaurant, evaluator|
        create_list(:reservation, evaluator.reservations_count, restaurant: restaurant)
      end
    end

    trait :with_menus do
      transient { menus_count { 1 } }
      after(:create) do |restaurant, evaluator|
        create_list(:menu, evaluator.menus_count, restaurant: restaurant)
      end
    end

    trait :with_tables do
      transient { tables_count { 1 } }
      after(:create) do |restaurant, evaluator|
        create_list(:table, evaluator.tables_count, restaurant: restaurant)
      end
    end

    factory :restaurant_with_reservations, traits: [:with_reservations]
    factory :restaurant_with_menus, traits: [:with_menus]
    factory :restaurant_with_tables, traits: [:with_tables]
  end

  factory :test_restaurant_model, class: Restaurant do
    name { 'Test Restaurant Name' }
    address { '00-670 Warszawa, Grodziecka 10' }
    description { 'Best Food' }
    guest_capacity { 1 }
    booking_capacity { 1 }
  end
end
