# frozen_string_literal: true

FactoryBot.define do
  factory :reservation, class: Reservation do
    name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    email { Faker::Internet.free_email }
    guest_number { 1 }
    table_id { 1 }

    association :restaurant, factory: :restaurant
  end

  factory :test_reservation_model, class: Reservation do
    name { 'FakeName' }
    surname { 'FakeSurname' }
    email { 'fake@email.com' }
    guest_number { 1 }
    table_id { 1 }
  end
end
