# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: User do
    name { Faker::Name.name }
    username { Faker::Name.name }
    email { Faker::Internet.free_email }
    password { 'Qwerty22' }
    password_confirmation { 'Qwerty22' }

    # has_many
    trait :with_restaurants do
      transient { restaurants_count { 2 } }
      after(:create) do |user, evaluator|
        create_list(:restaurant, evaluator.restaurants_count, user: user)
      end
    end

    trait :with_reservations do
      transient { restaurants_count { 1 } }
      transient { reservations_count { 5 } }
      after(:create) do |user, evaluator|
        create_list(:restaurant_with_reservations, evaluator.reservations_count, user: user)
      end
    end

    trait :with_menus do
      transient { restaurants_count { 1 } }
      transient { menus_count { 5 } }
      after(:create) do |user, evaluator|
        create_list(:restaurant_with_menus, evaluator.menus_count, user: user)
      end
    end

    factory :user_with_restaurants, traits: [:with_restaurants]
    factory :user_with_reservations, traits: [:with_reservations]
    factory :user_with_menus, traits: [:with_menus]
    factory :user_with_associations, traits: %i[with_restaurants with_reservations with_menus]
  end
end
