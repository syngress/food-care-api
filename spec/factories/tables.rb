# frozen_string_literal: true

FactoryBot.define do
  factory :table, class: Table do
    name { Faker::Name.name }
    capacity { 3 }
    restaurant_id { 1 }

    association :restaurant, factory: :restaurant
  end

  factory :test_table_model, class: Table do
    name { 'TableX' }
    capacity { 3 }
    restaurant_id { 1 }
  end
end
