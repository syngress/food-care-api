# frozen_string_literal: true

require 'rails_helper'

describe RestaurantsController, type: :routing do
  it { expect(get: '/restaurants').to route_to('restaurants#index') }
  it { expect(post: '/restaurants').to route_to('restaurants#create') }
  it { expect(get: '/restaurants/1').to route_to('restaurants#show', id: '1') }
  it { expect(put: '/restaurants/1').to route_to('restaurants#update', id: '1') }
  it { expect(delete: '/restaurants/1').to route_to('restaurants#destroy', id: '1') }
end
