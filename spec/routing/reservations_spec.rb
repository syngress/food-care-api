# frozen_string_literal: true

require 'rails_helper'

describe ReservationsController, type: :routing do
  it { expect(get: '/restaurants/1/reservations').to route_to('reservations#index', restaurant_id: '1') }
  it { expect(post: '/restaurants/1/reservations').to route_to('reservations#create', restaurant_id: '1') }
  it { expect(get: '/reservations/1').to route_to('reservations#show', id: '1') }
  it { expect(put: '/reservations/1').to route_to('reservations#update', id: '1') }
  it { expect(delete: '/reservations/1').to route_to('reservations#destroy', id: '1') }
end
