# frozen_string_literal: true

require 'rails_helper'

describe TablesController, type: :routing do
  it { expect(get: '/restaurants/1/tables').to route_to('tables#index', restaurant_id: '1') }
  it { expect(post: '/restaurants/1/tables').to route_to('tables#create', restaurant_id: '1') }
  it { expect(get: '/tables/1').to route_to('tables#show', id: '1') }
  it { expect(put: '/tables/1').to route_to('tables#update', id: '1') }
  it { expect(delete: '/tables/1').to route_to('tables#destroy', id: '1') }
end
