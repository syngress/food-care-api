# frozen_string_literal: true

require 'rails_helper'

describe MenusController, type: :routing do
  it { expect(get: '/restaurants/1/menus').to route_to('menus#index', restaurant_id: '1') }
  it { expect(post: '/restaurants/1/menus').to route_to('menus#create', restaurant_id: '1') }
  it { expect(get: '/menus/1').to route_to('menus#show', id: '1') }
  it { expect(put: '/menus/1').to route_to('menus#update', id: '1') }
  it { expect(delete: '/menus/1').to route_to('menus#destroy', id: '1') }
end
