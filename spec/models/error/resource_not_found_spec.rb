# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::ResourceNotFound do
  let(:message) { 'Resource not found' }
  subject { Error::ResourceNotFound.new(message) }

  it { expect(subject.key).to eq(:resource_not_found) }
  it { expect(subject.http_status.to_i).to eq(404) }
  it { expect(subject.message).to eq(message) }
end
