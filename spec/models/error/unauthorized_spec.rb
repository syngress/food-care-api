# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::Unauthorized do
  let(:message) { 'Signature has expired or invalid token' }
  subject { Error::Unauthorized.new(message) }

  it { expect(subject.key).to eq(:token_invalid) }
  it { expect(subject.http_status.to_i).to eq(401) }
  it { expect(subject.message).to eq(message) }
end
