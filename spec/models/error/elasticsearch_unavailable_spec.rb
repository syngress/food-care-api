# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::ElasticsearchUnavailable do
  subject { Error::ElasticsearchUnavailable.new }

  it 'should be equal to service_unavailable' do
    expect(subject.key).to eq(:service_unavailable)
  end

  it 'should return 503' do
    expect(subject.http_status.to_i).to eq(503)
  end
end
