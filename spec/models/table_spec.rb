# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Table, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('name') }
    it { is_expected.to include('capacity') }
  end

  describe '.new' do
    subject { build(:test_table_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.name).to eq('TableX')
      expect(subject.capacity).to eq(3)
    end
  end

  describe '#name' do
    subject { build(:test_table_model).name }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('TableX') }
  end

  describe '#capacity' do
    subject { build(:test_table_model).capacity }
    it { is_expected.to be_a(Integer) }
    it { is_expected.to eq(3) }
  end

  describe '#restaurnt_id' do
    subject { build(:test_table_model).restaurant_id }
    it { is_expected.to be_a(Integer) }
    it { is_expected.to eq(1) }
  end

  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:capacity) }
end
