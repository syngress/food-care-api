# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('name') }
    it { is_expected.to include('surname') }
    it { is_expected.to include('email') }
    it { is_expected.to include('guest_number') }
  end

  describe '.new' do
    subject { build(:test_reservation_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.name).to eq('FakeName')
      expect(subject.surname).to eq('FakeSurname')
      expect(subject.email).to eq('fake@email.com')
      expect(subject.guest_number).to eq(1)
    end
  end

  describe '#name' do
    subject { build(:test_reservation_model).name }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('FakeName') }
  end

  describe '#surname' do
    subject { build(:test_reservation_model).surname }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('FakeSurname') }
  end

  describe '#email' do
    subject { build(:test_reservation_model).email }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('fake@email.com') }
  end

  describe '#guest_number' do
    subject { build(:test_reservation_model).guest_number }
    it { is_expected.to be_a(Integer) }
    it { is_expected.to eq(1) }
  end

  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:surname) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:guest_number) }
end
