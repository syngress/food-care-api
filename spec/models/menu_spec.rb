# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Menu, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('category') }
    it { is_expected.to include('name') }
    it { is_expected.to include('description') }
    it { is_expected.to include('price') }
    it { is_expected.to include('currency') }
  end

  describe '.new' do
    subject { build(:test_menu_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.category).to eq('SomeCategory')
      expect(subject.name).to eq('SomeName')
      expect(subject.description).to eq('SomeDescription')
      expect(subject.price).to eq(13.11)
      expect(subject.currency).to eq('PLN')
    end
  end

  describe '#category' do
    subject { build(:test_menu_model).category }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('SomeCategory') }
  end

  describe '#name' do
    subject { build(:test_menu_model).name }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('SomeName') }
  end

  describe '#description' do
    subject { build(:test_menu_model).description }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('SomeDescription') }
  end

  describe '#price' do
    subject { build(:test_menu_model).price }
    it { is_expected.to be_a(BigDecimal) }
    it { is_expected.to eq(13.11) }
  end

  describe '#currency' do
    subject { build(:test_menu_model).currency }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('PLN') }
  end

  # Validation tests
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:currency) }
end
