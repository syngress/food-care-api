# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Restaurant, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('name') }
    it { is_expected.to include('address') }
    it { is_expected.to include('description') }
    it { is_expected.to include('guest_capacity') }
    it { is_expected.to include('booking_capacity') }
  end

  describe '.new' do
    subject { build(:test_restaurant_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.name).to eq('Test Restaurant Name')
      expect(subject.address).to eq('00-670 Warszawa, Grodziecka 10')
      expect(subject.description).to eq('Best Food')
      expect(subject.guest_capacity).to eq(1)
      expect(subject.booking_capacity).to eq(1)
    end
  end

  describe '#name' do
    subject { build(:test_restaurant_model).name }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('Test Restaurant Name') }
  end

  describe '#address' do
    subject { build(:test_restaurant_model).address }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('00-670 Warszawa, Grodziecka 10') }
  end

  describe '#description' do
    subject { build(:test_restaurant_model).description }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('Best Food') }
  end

  describe '#guest_capacity' do
    subject { build(:test_restaurant_model).guest_capacity }
    it { is_expected.to be_a(Integer) }
    it { is_expected.to eq(1) }
  end

  describe '#booking_capacity' do
    subject { build(:test_restaurant_model).booking_capacity }
    it { is_expected.to be_a(Integer) }
    it { is_expected.to eq(1) }
  end

  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:address) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:guest_capacity) }
  it { should validate_presence_of(:booking_capacity) }
end
