# frozen_string_literal: true

require 'rails_helper'

describe Restaurants::UpdateRestaurantService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  subject do
    described_class.new(
      form: form,
      restaurant: user.restaurants.first
    ).call
  end

  describe '#call' do
    context 'when restaurant form is valid and restaurant is updated' do
      let(:form) { Restaurants::UpdateRestaurantForm.new(guest_capacity: 20) }

      it 'perform form value validation' do
        expect(subject.object.guest_capacity).to eql(20)
      end
      it 'verifies if restaurant has been saved in the database' do
        subject
        expect(user.restaurants.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end
  end
end
