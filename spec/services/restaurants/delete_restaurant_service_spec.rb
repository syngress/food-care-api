# frozen_string_literal: true

require 'rails_helper'

describe Restaurants::DeleteRestaurantService, type: :service do
  let(:user) { create(:user, id_role: 1) }
  let(:user) { create(:user_with_restaurants, restaurants_count: 2, id_role: 1) }
  subject do
    described_class.new(
      user: user,
      restaurant: user.restaurants.first
    )
  end

  describe '#call' do
    context 'when restaurant is created' do
      it 'verifies if the menu has been saved in the database' do
        expect(user.restaurants.count).to eql(2)
      end
    end

    context 'when restaurant is deleted' do
      it 'perform delete object and verifies if restaurant has been deleted' do
        subject.call
        expect(user.restaurants.count).to eql(1)
      end
    end
  end
end
