# frozen_string_literal: true

require 'rails_helper'

describe Restaurants::CreateRestaurantService, type: :service do
  let(:user) { create(:user, id_role: 1) }
  subject do
    described_class.new(
      form: form,
      user: user
    ).call
  end

  describe '#call' do
    context 'when restaurant form is valid and restaurant is created' do
      let(:form) { Restaurants::CreateRestaurantForm.new(attributes_for(:restaurant)) }

      it 'perform form value validation' do
        expect(subject.object.name).to eql(form.name)
        expect(subject.object.address).to eql(form.address)
        expect(subject.object.description).to eql(form.description)
        expect(subject.object.guest_capacity).to eql(form.guest_capacity)
        expect(subject.object.booking_capacity).to eql(form.booking_capacity)
      end
      it 'verifies if the restaurant has been saved in the database' do
        subject
        expect(user.restaurants.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end

    context 'when restaurant form is invalid and restaurant is not created' do
      let(:form) { Restaurants::CreateRestaurantForm.new({}) }

      it {
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
        expect(user.restaurants.count).to eql(0)
      }
    end
  end
end
