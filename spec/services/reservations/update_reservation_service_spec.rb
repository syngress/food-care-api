# frozen_string_literal: true

require 'rails_helper'

describe Reservations::UpdateReservationService, type: :service do
  let(:restaurant) { create(:restaurant_with_tables, tables_count: 1, id: 1) }
  let(:reservation) { create(:reservation, restaurant_id: restaurant.id) }
  let(:table) { create(:table, restaurant_id: restaurant.id) }
  subject do
    described_class.new(
      form: form,
      reservation: reservation
    ).call
  end

  describe '#call' do
    context 'when reservation form is valid and reservation is updated' do
      let(:form) do
        Reservations::UpdateReservationForm.new(
          email: 'test@email.com',
          extend_time: 30,
          table_id: table.id,
          guest_number: 2
        )
      end

      it 'perform form value validation' do
        expect(subject.object.email).to eql('test@email.com')
      end
      it 'verifies if reservation has been saved in the database' do
        subject
        expect(restaurant.reservations.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end
  end
end
