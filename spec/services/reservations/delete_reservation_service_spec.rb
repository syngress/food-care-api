# frozen_string_literal: true

require 'rails_helper'

describe Reservations::DeleteReservationService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  let(:user) { create(:user_with_reservations, reservations_count: 2, id_role: 1) }
  subject do
    described_class.new(
      reservation: user.reservations.first
    )
  end

  describe '#call' do
    context 'when reservation is created' do
      it 'verifies if reservation has been saved in the database' do
        expect(user.reservations.count).to eql(2)
      end
    end

    context 'when resevation is deleted' do
      it 'perform delete object and verifies if reservation has been deleted' do
        subject.call
        expect(user.reservations.count).to eql(1)
      end
    end
  end
end
