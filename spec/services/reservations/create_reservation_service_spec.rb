# frozen_string_literal: true

require 'rails_helper'

describe Reservations::CreateReservationService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  let(:table) { create(:table, restaurant_id: user.restaurants.first.id, id: 1) }
  let(:reservation) { create(:reservation, restaurant_id: user.restaurants.first.id) }
  subject do
    form.objects.merge!(table_id: table.id, restaurant: user.restaurants.first)
    described_class.new(
      form: form,
      restaurant: user.restaurants.first
    ).call
  end

  describe '#call' do
    context 'when reservation form is valid and reservation is created' do
      let(:form) { Reservations::CreateReservationForm.new(attributes_for(:reservation)) }

      it 'perform form value validation' do
        if (Settings.global_time_window.end_hour..Settings.global_time_window.start_hour).to_a.include? Time.now.getlocal.hour
          expect(subject.object.name).to eql(form.name)
          expect(subject.object.surname).to eql(form.surname)
          expect(subject.object.email).to eql(form.email)
          expect(subject.object.guest_number).to eql(form.guest_number)
        else
          expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
        end
      end
      it 'verifies if the reservation has been saved in the database' do
        subject
        if (Settings.global_time_window.end_hour..Settings.global_time_window.start_hour).to_a.include? Time.now.getlocal.hour
          expect(user.restaurants.first.reservations.count).to eql(1)
        else
          expect(user.restaurants.first.reservations.count).to eql(0)
        end
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end

    context 'when restaurant form is invalid and reservation is not created' do
      let(:form) { Reservations::CreateReservationForm.new({}) }
      it 'checks for valid class instance' do
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
      end
      it 'verifies if the reservation has been saved in the database' do
        expect(user.restaurants.first.reservations.count).to eql(0)
      end
    end

    context 'when restaurant form is invalid and restaurant guest_number is exceeded' do
      let(:form) { Reservations::CreateReservationForm.new(name: 'SomeName', surname: 'SomeSurname', email: 'fake@email.com', guest_number: 20) }
      it 'checks for valid class instance' do
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
      end
      it 'verifies if the reservation has been saved in the database' do
        expect(user.restaurants.first.reservations.count).to eql(0)
      end
    end
  end
end
