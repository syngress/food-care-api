# frozen_string_literal: true

require 'rails_helper'

describe Menus::CreateMenuService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  subject do
    described_class.new(
      form: form,
      restaurant: user.restaurants.first
    ).call
  end

  describe '#call' do
    context 'when reservation form is valid and menu is created' do
      let(:form) { Menus::CreateMenuForm.new(attributes_for(:menu)) }

      it 'perform form value validation' do
        expect(subject.object.category).to eql(form.category)
        expect(subject.object.name).to eql(form.name)
        expect(subject.object.description).to eql(form.description)
        expect(subject.object.price).to eql(form.price)
        expect(subject.object.currency).to eql(form.currency)
      end
      it 'verifies if the menu has been saved in the database' do
        subject
        expect(user.menus.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end

    context 'when menu form is invalid and menu is not created' do
      let(:form) { Menus::CreateMenuForm.new({}) }
      it 'checks for valid class instance' do
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
      end
      it 'verifies if the menu has been saved in the database' do
        expect(user.menus.count).to eql(0)
      end
    end
  end
end
