# frozen_string_literal: true

require 'rails_helper'

describe Menus::UpdateMenuService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  let(:user) { create(:user_with_menus, menus_count: 1, id_role: 1) }
  subject do
    described_class.new(
      form: form,
      menu: user.menus.first
    ).call
  end

  describe '#call' do
    context 'when reservation form is valid and menu is updated' do
      let(:form) { Menus::UpdateMenuForm.new({ name: 'test_menu_item' }) }

      it 'perform form value validation' do
        expect(subject.object.name).to eql('test_menu_item')
      end
      it 'verifies if the menu has been saved in the database' do
        subject
        expect(user.menus.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end
  end
end
