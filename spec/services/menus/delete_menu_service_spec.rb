# frozen_string_literal: true

require 'rails_helper'

describe Menus::DeleteMenuService, type: :service do
  let(:user) { create(:user_with_restaurants, restaurants_count: 1, id_role: 1) }
  let(:user) { create(:user_with_menus, menus_count: 2, id_role: 1) }
  subject do
    described_class.new(
      menu: user.menus.first
    )
  end

  describe '#call' do
    context 'when menu is created' do
      it 'verifies if the menu has been saved in the database' do
        expect(user.menus.count).to eql(2)
      end
    end

    context 'when menu is deleted' do
      it 'perform delete object and verifies if menu has been deleted' do
        subject.call
        expect(user.menus.count).to eql(1)
      end
    end
  end
end
