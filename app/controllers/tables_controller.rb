# frozen_string_literal: true

# app/controllers/tables_controller.rb
class TablesController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_request, only: %i[index create update destroy]

  def show
    render json: Tables::TableSerializer.new(
      table
    ).serializable_hash
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: tables.count,
      restaurants: Tables::TableSerializer.new(
        tables
      ).serializable_hash
    }
  end

  def create
    form = Tables::CreateTableForm.new(table_params)
    result = Tables::CreateTableService.call(form: form, restaurant: restaurant)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Tables::TableSerializer
    end
  end

  def update
    form = Tables::UpdateTableForm.new(params_to_update, table: table)
    result = Tables::UpdateTableService.call(form: form, table: table, user: authorize_request)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Tables::TableSerializer
    end
  end

  def destroy
    result = Tables::DeleteTableService.call(table: table, user: authorize_request)
    render json: {
      message: "#{table.name} removed permanently from restaurant",
      object: result
    }
  end

  private

  def restaurant
    @restaurant ||= authorize_request.restaurants.find(params[:restaurant_id])
  end

  def table
    @table ||= Table.find(params[:id])
  end

  def tables
    @tables ||= Tables::GetTableService.call(
      params: params,
      user: authorize_request,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def table_params
    params.permit(:id, :name, :capacity).to_h
  end

  def sort_by_parameter
    Tables::ParamsForSort.allowed_values
  end

  def params_to_update
    ProcessUpdateParams.call(object: table, params: table_params)
  end
end
