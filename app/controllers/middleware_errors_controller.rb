# frozen_string_literal: true

# app/controllers/middleware_errors_controller.rb
class MiddlewareErrorsController < ActionController::API
  def show
    logger.fatal { { exception: { class: original_error.class.to_s, message: original_error.message, backtrace: original_error.backtrace.try(:first, 10) } } }
    render json: error, status: error.http_status
  end

  private

  def error
    Error::InternalServerError.new
  end

  def original_error
    request.env['action_dispatch.exception']
  end
end
