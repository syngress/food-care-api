# frozen_string_literal: true

# app/controllers/users_controller.rb
class UsersController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_request, except: :create
  before_action :user, except: %i[create index]

  def show
    render json: user,
           serializer: Users::UserSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: users.count,
      users: Users::UserSerializer.new(
        users
      ).serializable_hash
    }
  end

  def create
    form = Users::CreateUserForm.new(user_params)
    result = Users::CreateUserService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Users::UserSerializer
    end
  end

  def update
    form = Users::UpdateUserForm.new(params_to_update, user: user)
    result = Users::UpdateUserService.call(form: form, user: user)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Users::UserSerializer
    end
  end

  def destroy
    user.destroy
    render json: 'User deleted'
  end

  private

  def user
    @user ||= authorize_request.admin? ? User.find(params[:id]) : authorize_request
  end

  def users
    @users ||= Users::GetUserService.call(
      params: params,
      user: authorize_request,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def sort_by_parameter
    Users::ParamsForSort.allowed_values
  end

  def user_params
    params.permit(:id, :name, :username, :email, :password, :password_confirmation).to_h
  end

  def params_to_update
    ProcessUpdateParams.call(object: user, params: user_params)
  end
end
