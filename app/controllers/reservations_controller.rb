# frozen_string_literal: true

# app/controllers/reservation_controller.rb
class ReservationsController < ApplicationController
  include Pagination
  include Sorting

  before_action :reservation, only: %i[show update destroy]
  before_action :authorize_request, only: %i[show index update destroy]

  def show
    query = Reservations::ReservationSerializer.new(reservation).serializable_hash
    raise Error::ResourceNotFound if query[:data].empty?

    render json: Reservations::ReservationSerializer.new(reservation).serializable_hash
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: reservations.count,
      reservations: Reservations::ReservationSerializer.new(reservations).serializable_hash
    }
  end

  def create
    form = Reservations::CreateReservationForm.new(reservation_params, restaurant: restaurant)
    result = Reservations::CreateReservationService.call(form: form, restaurant: restaurant)
    if result.error?
      render_error(result.object)
    else
      render json: Reservations::ReservationSerializer.new(result.object).serializable_hash
    end
  end

  def update
    form = Reservations::UpdateReservationForm.new(params_to_update, reservation: reservation)
    result = Reservations::UpdateReservationService.call(form: form, reservation: reservation)
    if result.error?
      render_error(result.object)
    else
      render json: Reservations::ReservationSerializer.new(result.object).serializable_hash
    end
  end

  def destroy
    result = Reservations::DeleteReservationService.call(reservation: reservation)
    render json: {
      reservation: "your reservation no:#{reservation.id} has been removed permanently",
      object: result
    }
  end

  private

  def reservation
    @reservation ||= authorize_request.regular? ? authorize_request.reservations.where(id: params[:id]) : Reservation.find(params[:id])
  end

  def reservations
    @reservations ||= Reservations::GetReservationService.call(
      params: params,
      user: authorize_request,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def restaurant
    @restaurant ||= Restaurant.find(params[:restaurant_id])
  end

  def reservation_params
    params.permit(
      :id,
      :name,
      :surname,
      :email,
      :guest_number,
      :extend_time,
      :restaurant_id,
      :table_id
    ).to_h
  end

  def params_to_update
    ProcessUpdateParams.call(
      object: reservation,
      params: reservation_params,
      update_params: { table_id: reservation_params[:table_id] || reservation.table_id }
    )
  end
end
