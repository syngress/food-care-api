# frozen_string_literal: true

# app/controllers/restaurants_controller.rb
class RestaurantsController < ApplicationController
  include Pagination
  include Sorting

  before_action :restaurant, only: %i[show update destroy]
  before_action :authorize_request, only: %i[create update destroy]

  def show
    render json: Restaurants::RestaurantSerializer.new(
      restaurant
    ).serializable_hash
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      records: restaurants.count,
      restaurants: Restaurants::RestaurantSerializer.new(
        restaurants
      ).serializable_hash
    }
  end

  def create
    form = Restaurants::CreateRestaurantForm.new(restaurant_params)
    result = Restaurants::CreateRestaurantService.call(form: form, user: authorize_request)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Restaurants::RestaurantSerializer
    end
  end

  def update
    form = Restaurants::UpdateRestaurantForm.new(params_to_update, restaurant: restaurant)
    result = Restaurants::UpdateRestaurantService.call(form: form, restaurant: restaurant)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Restaurants::RestaurantSerializer
    end
  end

  def destroy
    result = Restaurants::DeleteRestaurantService.call(user: authorize_request, restaurant: restaurant)
    render json: {
      restaurant: "#{restaurant.name} removed permanently",
      object: result
    }
  end

  private

  def restaurant
    @restaurant ||= Restaurant.find(params[:id])
  end

  def restaurants
    @restaurants ||= Restaurants::GetRestaurantService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def sort_by_parameter
    Restaurants::ParamsForSort.allowed_values
  end

  def restaurant_params
    params.permit(:id, :name, :address, :description, :guest_capacity, :booking_capacity).to_h
  end

  def params_to_update
    ProcessUpdateParams.call(object: restaurant, params: restaurant_params)
  end
end
