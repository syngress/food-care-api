# frozen_string_literal: true

# app/controllers/menus_controller.rb
class MenusController < ApplicationController
  include Pagination
  include Sorting

  before_action :menu, only: %i[show update destroy]
  before_action :authorize_request, only: %i[create update destroy]

  def show
    render json: menu, serializer: Menus::MenuSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: menus.count,
      menus: Menus::MenuSerializer.new(
        menus
      ).serializable_hash
    }
  end

  def create
    form = Menus::CreateMenuForm.new(menu_params, restaurant: restaurant)
    result = Menus::CreateMenuService.call(form: form, restaurant: restaurant)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Menus::MenuSerializer
    end
  end

  def update
    form = Menus::UpdateMenuForm.new(params_to_update, menu: menu)
    result = Menus::UpdateMenuService.call(form: form, menu: menu)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Menus::MenuSerializer
    end
  end

  def destroy
    result = Menus::DeleteMenuService.call(menu: menu)
    render json: {
      menu: "your menu item number #{menu.id} has been removed permanently",
      object: result
    }
  end

  private

  def menu
    @menu ||= authorize_request.regular? ? authorize_request.menus.find(params[:id]) : Menu.find(params[:id])
  end

  def menus
    @menus ||= Menus::GetMenuService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def restaurant
    @restaurant ||= Restaurant.find(params[:restaurant_id])
  end

  def menu_params
    params.permit(:id, :category, :name, :description, :price, :currency).to_h
  end

  def params_to_update
    ProcessUpdateParams.call(object: menu, params: menu_params)
  end
end
