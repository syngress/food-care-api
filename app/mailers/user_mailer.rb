# frozen_string_literal: true

# app/mailers/user_mailer.rb
class UserMailer < ActionMailer::Base
  default from: 'pclabcc@gmail.com'

  def welcome(email, restaurant_name)
    Rails.logger.info "== sending welcome email to ==> #{email}"
    mail(to: email, subject: "See You Next Time In #{restaurant_name}")
  end
end
