# frozen_string_literal: true

# app/validators/uniqueness_validator.rb
class UniquenessValidator < ActiveRecord::Validations::UniquenessValidator
  def initialize(klass)
    super
    @klass = options[:model] if options[:model]
  end

  def validate_each(record, attribute, value)
    raise ArgumentError, "Unknown validator: 'UniquenessValidator'" if !options[:model] && !record.class.ancestors.include?(ActiveRecord::Base)

    super unless options[:model]

    record_org = record
    attribute_org = attribute
    attribute = options[:attribute].to_sym if options[:attribute]
    scope = options[:scope]

    attributes = { attribute => value }
    attributes.merge!(scope => record.send(scope)) if scope.present?
    record = options[:model].new(attributes)

    super

    record_org.errors.add(attribute_org, :not_unique) if record.errors.any?
  end
end
