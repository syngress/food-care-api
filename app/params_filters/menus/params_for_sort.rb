# frozen_string_literal: true

module Menus
  # app/params_filters/menus/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id category name description price currency]
      end
    end
  end
end
