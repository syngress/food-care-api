# frozen_string_literal: true

module Menus
  # /app/params_filters/menus/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id category name description price currency]
      end
    end
  end
end
