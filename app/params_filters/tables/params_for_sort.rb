# frozen_string_literal: true

module Tables
  # app/params_filters/tables/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name capacity]
      end
    end
  end
end
