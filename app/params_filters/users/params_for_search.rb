# frozen_string_literal: true

module Users
  # /app/params_filters/users/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id name username email]
      end
    end
  end
end
