# frozen_string_literal: true

module Restaurants
  # /app/params_filters/restaurants/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id name address description guest_capacity booking_capacity]
      end
    end
  end
end
