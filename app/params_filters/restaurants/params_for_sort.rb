# frozen_string_literal: true

module Restaurants
  # app/params_filters/restaurants/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name address description guest_capacity booking_capacity]
      end
    end
  end
end
