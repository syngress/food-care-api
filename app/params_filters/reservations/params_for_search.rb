# frozen_string_literal: true

module Reservations
  # /app/params_filters/reservations/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id name surname guest_number]
      end
    end
  end
end
