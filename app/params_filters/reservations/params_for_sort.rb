# frozen_string_literal: true

module Reservations
  # app/params_filters/reservations/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name surname guest_number]
      end
    end
  end
end
