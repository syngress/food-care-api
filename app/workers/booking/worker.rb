# frozen_string_literal: true

module Booking
  # app/workers/booking/worker.rb
  class Worker
    include Sidekiq::Worker
    sidekiq_options retry: 3
    sidekiq_retries_exhausted do |msg, _error|
      batch = Sidekiq::Batch.new(msg['bid'])
      batch.invalidate_all
    end

    def perform(email, restaurant_name)
      UserMailer.welcome(email, restaurant_name).deliver
    end
  end
end
