# frozen_string_literal: true

module Booking
  # app/workers/booking/booking_success_worker.rb
  class BookingSuccessWorker
    include Sidekiq::Worker

    def on_success(_status, options)
      puts 'Send booking message to kafka queue'

      Kafka::DataSerializers::Email::SendEmailMessage.new(
        {
          email: options['email'],
          subject: 'See You Next Time',
          restaurant_name: options['restaurant_name'],
          message: "See You Next Time In #{options['restaurant_name']}.",
          dispatch_time: Time.now.to_i
        }
      ).call
    end
  end
end
