# frozen_string_literal: true

module Booking
  # app/workers/booking/booking_worker.rb
  class BookingWorker
    include Sidekiq::Worker
    sidekiq_options queue: 'batches'

    def perform(email, restaurant_name)
      batch = Sidekiq::Batch.new
      batch.description = 'Send reservation email to customer'
      batch.on(:success, Booking::BookingSuccessWorker, { email: email, restaurant_name: restaurant_name })
      batch.jobs do
        Worker.perform_async(email, restaurant_name)
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        raise Error::Unauthorized, e
      end
    end
  end
end
