# frozen_string_literal: true

module Elasticsearch
  module Index
    # app/workers/elasticsearch/index/reservation_worker.rb
    class ReservationWorker < Worker
      sidekiq_options queue: :elasticsearch

      def perform(attrs)
        return if Settings.elasticsearch.queue_disabled?

        if Settings.elasticsearch.enabled?
          record = ::Reservation.find(attrs.fetch('record_id')).__elasticsearch__
          Elasticsearch::Index::ManifestDocument.call(method: attrs.fetch('method'), record: record)
        end
      rescue StandardError => e
        puts e
      end
    end
  end
end
