# frozen_string_literal: true

module Tables
  # /app/serializers/tables/table_serializer.rb
  class TableSerializer
    include FastJsonapi::ObjectSerializer

    belongs_to :restaurant, links: {
      restaurant: ->(object) { "#{Settings.host}/restaurants/#{object.restaurant_id}" }
    }

    attributes :id, :name, :capacity, :created_at, :updated_at
  end
end
