# frozen_string_literal: true

module Reservations
  # /app/serializers/reservations/reservation_serializer.rb
  class ReservationSerializer
    include FastJsonapi::ObjectSerializer

    attributes :id, :name, :surname, :guest_number, :email, :extend_time, :table_id, :created_at, :updated_at

    attribute :reservation_time do |object|
      object.extend_time.nil? ? 120 : object.extend_time + 120
    end

    attribute :extend_time do |object|
      object.extend_time.nil? ? 0 : object.extend_time
    end
  end
end
