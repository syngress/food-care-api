# frozen_string_literal: true

module Kafka
  module ToAvro
    # app/serializers/kafka/to_avro/base.rb
    class Base
      attr_reader :model

      def initialize(model)
        @model = model
      end

      def call
        encode(message)
      end

      private

      def message
        {
          params: {
            schema_name: schema_name,
            namespace: namespace,
            subject: subject
          },
          data: data.as_json
        }
      end

      def encode(message)
        $avro.encode(
          message[:data],
          schema_name: message[:params][:schema_name],
          namespace: message[:params][:namespace],
          subject: message[:params][:subject]
        )
      end
    end
  end
end
