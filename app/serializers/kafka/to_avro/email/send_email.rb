# frozen_string_literal: true

module Kafka
  module ToAvro
    module Email
      # app/serializers/kafka/to_avro/email/send_email.rb
      class SendEmail < Base
        def schema_name
          'send_email_message'
        end

        def namespace
          'com.foodcare.model'
        end

        def subject
          'email'
        end

        def data
          {
            payload: {
              email: model[:email],
              subject: model[:subject],
              restaurant_name: model[:restaurant_name],
              message: model[:message],
              dispatch_time: Time.now.to_i
            }
          }
        end
      end
    end
  end
end
