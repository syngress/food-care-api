# frozen_string_literal: true

module Restaurants
  # /app/serializers/restaurants/restaurant_serializer.rb
  class RestaurantSerializer
    include FastJsonapi::ObjectSerializer

    has_many :menus, links: {
      menu: ->(object) { "#{Settings.host}/restaurants/#{object.id}/menus" }
    }

    has_many :tables, links: {
      table: ->(object) { "#{Settings.host}/restaurants/#{object.id}/tables" }
    }

    attributes :id, :name, :address, :description, :guest_capacity, :booking_capacity, :created_at, :updated_at
  end
end
