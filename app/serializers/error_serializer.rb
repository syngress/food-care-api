# frozen_string_literal: true

# /app/serializers/error_serializer.rb
class ErrorSerializer
  include FastJsonapi::ObjectSerializer
  set_key_transform :underscore

  attributes :status, :error, :message, :details

  def error
    object.key
  end

  def status
    object.http_status.to_i
  end
end
