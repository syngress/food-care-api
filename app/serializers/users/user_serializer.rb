# frozen_string_literal: true

module Users
  # /app/serializers/users/user_serializer.rb
  class UserSerializer
    include FastJsonapi::ObjectSerializer

    attributes :id, :role, :name, :username, :email, :password_digest, :created_at, :updated_at

    attribute :role do |object|
      object.id_role == 1 ? 'regular' : 'administrator'
    end
  end
end
