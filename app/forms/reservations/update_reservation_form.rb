# frozen_string_literal: true

module Reservations
  # app/forms/reservations/update_reservation_form.rb
  class UpdateReservationForm < Form
    attribute :name, String
    attribute :surname, String
    attribute :email, String
    attribute :guest_number, Integer
    attribute :extend_time, Integer
    attribute :table_id, Integer

    validates :name,
              length: { maximum: 255 },
              if: -> { keys.include?(:name) }

    validates :surname,
              length: { maximum: 255 },
              if: -> { keys.include?(:surname) }

    validates :email,
              length: { maximum: 255 },
              if: -> { keys.include?(:email) }

    validates :guest_number,
              numericality: { only_integer: true, greater_than: 0 },
              if: -> { keys.include?(:guest_number) }

    validates :extend_time,
              numericality: { only_integer: true, greater_than: 0, less_than: 360, allow_blank: false },
              if: -> { keys.include?(:extend_time) }

    validates :table_id,
              numericality: { only_integer: true, greater_than: 0 },
              if: -> { keys.include?(:table_id) }

    validate :number_of_guests_for_table
    validate :extended_time

    private

    def number_of_guests_for_table
      table = Table.where(id: table_id).last
      if table.nil?
        errors.add(:reservation, I18n.t(:unknown_table, scope: 'errors.reservation'))
      elsif guest_number > table.capacity
        errors.add(:reservation, I18n.t(:table_guests_exceeded, scope: 'errors.reservation'))
      end
    end

    def extended_time
      if extend_time.nil? || extend_time > 360
        errors.add(:reservation, I18n.t(:extend_time_exceeded, scope: 'errors.reservation'))
      else
        end_of_work_hour = Settings.global_time_window.start_hour
        end_of_workday = DateTime.new(Time.now.year, Time.now.month, Time.now.day, end_of_work_hour, 00)
        errors.add(:reservation, I18n.t(:extend_time_outside_workday, scope: 'errors.reservation')) if Time.now + extend_time.minutes > end_of_workday
      end
    end
  end
end
