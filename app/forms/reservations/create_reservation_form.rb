# frozen_string_literal: true

module Reservations
  # app/forms/reservations/create_reservation_form.rb
  class CreateReservationForm < Form
    attribute :name, String
    attribute :surname, String
    attribute :email, String
    attribute :guest_number, Integer
    attribute :extend_time, Integer
    attribute :table_id, Integer

    validates :name,
              presence: true

    validates :surname,
              presence: true

    validates :email,
              presence: true

    validates :guest_number,
              presence: true,
              numericality: { only_integer: true, greater_than: 0 }

    validates :extend_time,
              presence: true,
              numericality: { only_integer: true, greater_than: 0, less_than: 360, allow_blank: false },
              if: -> { keys.include?(:extend_time) }

    validates :table_id,
              presence: true,
              numericality: { only_integer: true, greater_than: 0 }

    validate :reservation_limits
    validate :booking_time_window
    validate :extended_time, unless: -> { extend_time.nil? }
    validate :number_of_guests_for_table
    validate :reservation_availability

    private

    def reservation_limits
      if one_day_reservations.present?
        if one_day_reservations.count > objects[:restaurant].booking_capacity
          errors.add(:reservation, I18n.t(:limit_exceeded, scope: 'errors.reservation'))
        end
        if one_day_reservations.sum(:guest_number) + guest_number > objects[:restaurant].guest_capacity
          errors.add(:guest, I18n.t(:limit_exceeded, scope: 'errors.guest'))
        end
      elsif guest_number.present? && guest_number > objects[:restaurant].guest_capacity
        errors.add(:guest, I18n.t(:limit_exceeded, scope: 'errors.guest'))
      end
    end

    def booking_time_window
      booking_time_window_error unless (Settings.global_time_window.end_hour..Settings.global_time_window.start_hour).to_a.include? Time.now.getlocal.hour
    end

    def booking_time_window_error
      errors.add(
        :reservation,
        I18n.t(
          :booking_time_window,
          scope: 'errors.reservation',
          start_hour: Settings.global_time_window.start_hour,
          end_hour: Settings.global_time_window.end_hour
        )
      )
    end

    def extended_time
      if extend_time > 360
        errors.add(:reservation, I18n.t(:extend_time_exceeded, scope: 'errors.reservation'))
      else
        end_of_work_hour = Settings.global_time_window.start_hour
        end_of_workday = DateTime.new(Time.now.year, Time.now.month, Time.now.day, end_of_work_hour, 00)
        errors.add(:reservation, I18n.t(:extend_time_outside_workday, scope: 'errors.reservation')) if Time.now + extend_time.minutes > end_of_workday
      end
    end

    def number_of_guests_for_table
      table = Table.where(id: table_id).last
      if table.nil? || !table_id.in?(objects[:restaurant].tables.map(&:id))
        errors.add(:reservation, I18n.t(:unknown_table, scope: 'errors.reservation'))
      elsif guest_number > table.capacity
        errors.add(:reservation, I18n.t(:table_guests_exceeded, scope: 'errors.reservation'))
      end
    end

    def reservation_availability
      one_day_reservations.where(table_id: table_id).each do |reservation|
        errors.add(:reservation, I18n.t(:table_unavailable, scope: 'errors.reservation', minutes: table_available_at(reservation))) && break if
          format('%.0f', (Time.now.to_time - reservation.created_at) / 1.minutes).to_f < Settings.default_reservation_time + extend_time
      end
    end

    def table_available_at(reservation)
      # Default reservation time is 120 minutes
      (Settings.default_reservation_time + extend_time) - format('%.0f', (Time.now.to_time - reservation.created_at) / 1.minutes).to_i
    end

    def one_day_reservations
      @one_day_reservations ||= objects[:restaurant].reservations.where(created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
    end
  end
end
