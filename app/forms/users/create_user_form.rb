# frozen_string_literal: true

module Users
  # app/forms/users/create_user_form.rb
  class CreateUserForm < Form
    attribute :name, String
    attribute :username, String
    attribute :email, String
    attribute :password, String
    attribute :password_confirmation, String

    validates :name,
              presence: true

    validates :username,
              presence: true,
              uniqueness: { model: User }

    validates :email,
              presence: true,
              format: { with: URI::MailTo::EMAIL_REGEXP },
              uniqueness: { model: User }

    validates :password,
              presence: true,
              length: { minimum: 6 }

    validates :password_confirmation,
              presence: true

    validate :password_match, if: -> { password.present? }

    private

    def password_match
      errors.add(:password, I18n.t(:match_error, scope: 'errors.attributes.password')) if password != password_confirmation
    end
  end
end
