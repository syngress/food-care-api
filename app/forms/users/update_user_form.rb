# frozen_string_literal: true

module Users
  # app/forms/users/update_user_form.rb
  class UpdateUserForm < Form
    config Settings.form.users.update_user

    attribute :name, String
    attribute :username, String
    attribute :email, String
    attribute :password, String
    attribute :password_confirmation, String

    validates :name,
              presence: false

    validates :username,
              presence: false,
              uniqueness: { model: User },
              if: :validate_username_uniqueness

    validates :email,
              presence: false,
              format: { with: /#{settings.email.format}/ },
              length: { maximum: settings.email.max_length },
              uniqueness: { model: User },
              if: :validate_email_uniqueness

    validates :password,
              presence: true,
              length: { minimum: 6 },
              if: -> { !password.nil? }

    validates :password_confirmation,
              presence: true,
              if: -> { !password.nil? }

    def to_hash
      super.compact
    end

    def user
      @user ||= objects[:user]
    end

    def validate_username_uniqueness
      !username.nil? && user.try(:username) != username
    end

    def validate_email_uniqueness
      !email.nil? && user.try(:email) != email
    end
  end
end
