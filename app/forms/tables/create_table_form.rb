# frozen_string_literal: true

module Tables
  # app/forms/tables/create_table_form.rb
  class CreateTableForm < Form
    attribute :name, String
    attribute :capacity, Integer

    validates :name,
              length: { maximum: 255 },
              presence: true,
              uniqueness: { model: Table }

    validates :capacity,
              numericality: { greater_than: 0, only_integer: true },
              presence: true
  end
end
