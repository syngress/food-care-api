# frozen_string_literal: true

module Tables
  # app/forms/tables/update_table_form.rb
  class UpdateTableForm < Form
    attribute :name, String
    attribute :capacity, Integer

    validates :name,
              length: { maximum: 255 },
              uniqueness: { model: Table },
              if: :validate_name_uniqueness

    validates :capacity,
              numericality: { only_integer: true },
              if: -> { keys.include?(:capacity) }

    def validate_name_uniqueness
      !name.nil? && objects[:table].try(:name) != name
    end
  end
end
