# frozen_string_literal: true

module Menus
  # app/forms/menus/create_menu_form.rb
  class CreateMenuForm < Form
    attribute :category, String
    attribute :name, String
    attribute :description, String
    attribute :price, Decimal
    attribute :currency, String, default: Settings.currency.default

    validates :category,
              presence: true

    validates :name,
              presence: true

    validates :description,
              presence: true

    validates :price,
              presence: true,
              numericality: { greater_than: 0 }

    validates :currency,
              inclusion: Settings.currency.whitelist
  end
end
