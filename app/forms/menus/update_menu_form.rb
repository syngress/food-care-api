# frozen_string_literal: true

module Menus
  # app/forms/menus/update_menu_form.rb
  class UpdateMenuForm < Form
    attribute :category, String
    attribute :name, String
    attribute :description, String
    attribute :price, Decimal
    attribute :currency, String

    validates :category,
              length: { maximum: 255 },
              if: -> { keys.include?(:category) }

    validates :name,
              length: { maximum: 255 },
              if: -> { keys.include?(:name) }

    validates :description,
              length: { maximum: 255 },
              if: -> { keys.include?(:description) }

    validates :price,
              numericality: { greater_than: 0 },
              if: -> { keys.include?(:price) }

    validates :currency,
              length: { maximum: 255 },
              inclusion: Settings.currency.whitelist,
              if: -> { keys.include?(:currency) }
  end
end
