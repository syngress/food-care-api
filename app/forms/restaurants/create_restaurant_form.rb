# frozen_string_literal: true

module Restaurants
  # app/forms/restaurants/create_restaurant_form.rb
  class CreateRestaurantForm < Form
    attribute :name, String
    attribute :address, String
    attribute :description, String
    attribute :guest_capacity, Integer
    attribute :booking_capacity, Integer

    validates :name,
              length: { maximum: 255 },
              presence: true,
              uniqueness: { model: Restaurant }

    validates :address,
              length: { maximum: 255 },
              presence: true

    validates :description,
              length: { maximum: 255 },
              presence: true

    validates :guest_capacity,
              numericality: { greater_than: 0, only_integer: true },
              presence: true

    validates :booking_capacity,
              numericality: { greater_than: 0, only_integer: true },
              presence: true
  end
end
