# frozen_string_literal: true

module Restaurants
  # app/forms/restaurants/update_restaurant_form.rb
  class UpdateRestaurantForm < Form
    attribute :name, String
    attribute :address, String
    attribute :description, String
    attribute :guest_capacity, Integer
    attribute :booking_capacity, Integer

    validates :name,
              length: { maximum: 255 },
              uniqueness: { model: Restaurant },
              if: :validate_name_uniqueness

    validates :address,
              length: { maximum: 255 },
              if: -> { keys.include?(:address) }

    validates :description,
              length: { maximum: 255 },
              if: -> { keys.include?(:description) }

    validates :guest_capacity,
              numericality: { only_integer: true },
              if: -> { keys.include?(:guest_capacity) }

    validates :booking_capacity,
              numericality: { only_integer: true },
              if: -> { keys.include?(:booking_capacity) }

    def validate_name_uniqueness
      !name.nil? && objects[:restaurant].try(:name) != name
    end
  end
end
