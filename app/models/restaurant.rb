# frozen_string_literal: true

# app/models/restaurant.rb
class Restaurant < ApplicationRecord
  # model association
  has_many :reservations, foreign_key: :restaurant_id, dependent: :destroy
  has_many :menus, foreign_key: :restaurant_id, dependent: :destroy
  has_many :tables, foreign_key: :restaurant_id, dependent: :destroy
  belongs_to :user

  # validations
  validates_presence_of :name, :address, :description, :guest_capacity, :booking_capacity
  validates :name, uniqueness: true
end
