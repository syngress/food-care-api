# frozen_string_literal: true

# app/models/error_event.rb
class ErrorEvent < ApplicationRecord
  # model association
  belongs_to :restaurant

  # validations
  validates_presence_of :cok, :error_id, :error_description, :error_date

  # scopes
  scope :id, (->(id) { where id: id })
  scope :cok, (->(cok) { where cok: cok })
  scope :error_id, (->(error_id) { where error_id: error_id })
  scope :error_description, (->(error_description) { where error_description: error_description })
  scope :error_date, (->(error_date) { where error_date: error_date })
  scope :created_at, (->(created_at) { where created_at: created_at })
end
