# frozen_string_literal: true

# app/models/user.rb
class User < ApplicationRecord
  has_secure_password

  # model association
  has_many :restaurants, foreign_key: :user_id, dependent: :destroy
  has_many :reservations, through: :restaurants, dependent: :destroy
  has_many :menus, through: :restaurants, dependent: :destroy

  validates :email, uniqueness: true
  validates :username, uniqueness: true
  validates_presence_of :name, :username, :email, :password_digest

  ROLES = {
    REGULAR: 1,
    ADMIN: 2
  }.freeze

  def regular?
    id_role == ROLES[:REGULAR]
  end

  def admin?
    id_role == ROLES[:ADMIN]
  end
end
