# frozen_string_literal: true

class Menu < ApplicationRecord
  # model association
  belongs_to :restaurant

  # validations
  validates_presence_of :category, :name, :description, :price, :currency
end
