# frozen_string_literal: true

class Error
  # /app/models/error/generic.rb
  class Generic < Error
    def initialize(message, key, http_status = nil)
      super(message, key, {}, Http::Status.new(http_status))
    end
  end
end
