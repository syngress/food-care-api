# frozen_string_literal: true

class Error
  # /app/models/error/elasticsearch_unavailable.rb
  class ElasticsearchUnavailable < Error
    def initialize
      message = I18n.t(:service_unavailable, scope: 'errors.messages')
      super(message, :service_unavailable, {}, Http::Status.new(503))
    end
  end
end
