# frozen_string_literal: true

class Error
  # /app/models/error/invalid_action.rb
  class InvalidAction < Error
    def initialize(options = nil)
      http_status = Http::Status.new(400)
      details = options[:details]
      message = I18n.t('errors.messages.invalid_action')

      super(message, :invalid_action, details, http_status)
    end
  end
end
