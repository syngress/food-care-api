# frozen_string_literal: true

class Error
  # /app/models/error/internal_server_error.rb
  class InternalServerError < Error
    def initialize(details = nil)
      message = I18n.t(:internal_server_error, scope: 'locale.messages')
      super(message, :internal_server_error, details, Http::Status.new(500))
    end
  end
end
