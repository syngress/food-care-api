# frozen_string_literal: true

class Reservation < ApplicationRecord
  include Reservations::Concern

  # model association
  belongs_to :restaurant

  # validations
  validates_presence_of :name, :surname, :email, :guest_number, :table_id
end
