# frozen_string_literal: true

# app/models/error.rb
class Error < StandardError
  include ActiveModel::Serialization

  attr_reader :key, :details, :http_status

  def initialize(message, key, details = {}, http_status = nil)
    raise ArgumentError, I18n.t(:empty_error_message, scope: 'errors.messages') if message.blank?
    raise ArgumentError, I18n.t(:error_key_nil, scope: 'errors.messages') if key.blank?

    unless http_status.nil? || http_status.is_a?(Http::Status)
      raise ArgumentError, I18n.t(:http_status_instance, scope: 'errors.messages', http_status: Http::Status)
    end

    super(message)
    @key = key.to_sym
    @details = details
    @http_status = http_status
  end

  def as_json(_args = nil)
    {
      status: http_status.status,
      error: key,
      message: message,
      details: details
    }
  end
end
