# frozen_string_literal: true

module Reservations
  # app/models/concerns/reservations/concern.rb
  module Concern
    extend ActiveSupport::Concern

    if Settings.elasticsearch.enabled?
      included do
        include Elasticsearch::Model
        include Elasticsearch::Model::Callbacks

        index_name Settings.elasticsearch.index_alias

        after_commit on: [:create] do
          Elasticsearch::Index::Reservation.call(method: :create, record: self, restaurant: restaurant)
        end

        after_commit on: [:update] do
          Elasticsearch::Index::Reservation.call(method: :update, record: self, restaurant: restaurant)
        end

        after_commit on: [:destroy] do
          Elasticsearch::Index::Reservation.call(method: :destroy, record: self, restaurant: restaurant)
        end

        def as_indexed_json(_options)
          as_json(only: %i[id name surname email guest_number restaurant_id created_at])
        end
      end
    end
  end
end
