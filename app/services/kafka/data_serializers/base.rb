# frozen_string_literal: true

module Kafka
  module DataSerializers
    # app/services/kafka/data_serializers/base.rb
    class Base
      def initialize(object)
        @object = object
      end

      def call
        return unless Settings.kafka.sending_enabled

        responder.call(message)
      end

      private

      def message
        encoder.new(object).call
      end

      attr_reader :object
    end
  end
end
