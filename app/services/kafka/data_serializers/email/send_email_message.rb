# frozen_string_literal: true

module Kafka
  module DataSerializers
    module Email
      # app/services/kafka/data_serializers/email/send_email_message.rb
      class SendEmailMessage < ::Kafka::DataSerializers::Base
        def responder
          Kafka::Responders::EmailResponder
        end

        def encoder
          Kafka::ToAvro::Email::SendEmail
        end
      end
    end
  end
end
