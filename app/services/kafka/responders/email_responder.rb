# frozen_string_literal: true

module Kafka
  module Responders
    # app/services/kafka/responders/email_responder.rb
    class EmailResponder < ApplicationResponder
      topic Settings.kafka.topics.email

      def call(message)
        super(Settings.kafka.topics.email, message)
      end
    end
  end
end
