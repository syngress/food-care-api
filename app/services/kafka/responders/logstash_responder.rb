# frozen_string_literal: true

module Kafka
  module Responders
    # app/services/kafka/responders/logstash_responder.rb
    class LogstashResponder < ApplicationResponder
      topic Settings.kafka.topics.logstash, async: true

      def respond(message)
        respond_to(Settings.kafka.topics.logstash, message)
      end
    end
  end
end
