# frozen_string_literal: true

module Kafka
  # app/services/kafka/application_consumer.rb
  class ApplicationConsumer < Karafka::BaseConsumer
    def with_connection
      attempts_count ||= 0
      attempts_count += 1
      yield
    rescue ActiveRecord::StatementInvalid => e
      ActiveRecord::Base.clear_active_connections!
      attempts_count <= 3 ? retry : raise(e)
    end
  end
end
