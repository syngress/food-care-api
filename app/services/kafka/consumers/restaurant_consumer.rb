# frozen_string_literal: true

module Kafka
  module Consumers
    # app/services/kafka/consumers/restaurant_consumer.rb
    class RestaurantConsumer < ApplicationConsumer
      def consume
        attrs = {
          name: params['name'],
          address: params['address'],
          description: params['description'],
          guest_capacity: params['guest_capacity'],
          booking_capacity: params['booking_capacity']
        }.compact

        with_connection do
          restaurant = Restaurant.find_by(name: params['name'])
          if restaurant.present?
            restaurant.update(attrs)
          else
            Restaurant.create(attrs)
          end
        end
      end
    end
  end
end
