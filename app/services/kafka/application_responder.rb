# frozen_string_literal: true

module Kafka
  # app/services/kafka/application_responder.rb
  class ApplicationResponder < Karafka::BaseResponder
    def respond(topic, message)
      respond_to(topic, message, create_time: Time.now)
    end
  end
end
