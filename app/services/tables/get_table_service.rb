# frozen_string_literal: true

module Tables
  # /app/services/tables/get_table_service.rb
  class GetTableService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
      @user = attrs.fetch(:user)
    end

    def call
      tables_collection
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :user, :params

    def tables_collection
      user.admin? ? all_tables : authorized_tables
    end

    def authorized_tables
      restaurant = user.restaurants.find(params[:restaurant_id])
      Table.where(restaurant_id: restaurant.id).load_async.order(order).page(page).per(per_page)
    end

    def all_tables
      Table.all.load_async.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
