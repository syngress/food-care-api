# frozen_string_literal: true

module Tables
  # /app/services/tables/delete_table_service.rb
  class DeleteTableService
    include Service

    def initialize(attrs = {})
      @table = attrs.fetch(:table)
      @user = attrs.fetch(:user)
    end

    def call
      delete_restaurant_table
    end

    private

    attr_reader :table, :user

    def delete_restaurant_table
      user.admin? ? table.destroy : check_authorized_and_delete
    end

    def check_authorized_and_delete
      raise Error::Unauthorized if table.restaurant.user_id != user.id

      table.destroy
    end
  end
end
