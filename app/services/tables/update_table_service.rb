# frozen_string_literal: true

module Tables
  # /app/services/tables/update_table_service.rb
  class UpdateTableService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @table = attrs.fetch(:table)
      @user = attrs.fetch(:user)
    end

    def call
      check_from
      check_user
      update_restaurant_table
    end

    private

    attr_reader :form, :table, :user

    def check_from
      return build_result(validation_error) unless form.valid?
    end

    def check_user
      raise Error::Unauthorized if user.id != table.restaurant.user.id && !user.admin?
    end

    def update_restaurant_table
      table.update(form.to_hash)
      build_result(table)
    end
  end
end
