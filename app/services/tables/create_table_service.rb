# frozen_string_literal: true

module Tables
  # /app/services/tables/create_table_service.rb
  class CreateTableService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @restaurant = attrs.fetch(:restaurant)
    end

    def call
      return build_result(validation_error) unless form.valid?

      build_result(table)
    end

    private

    attr_reader :form, :restaurant

    def table
      @table ||= restaurant.tables.create(form.to_hash)
    end
  end
end
