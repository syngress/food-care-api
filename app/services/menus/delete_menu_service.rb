# frozen_string_literal: true

module Menus
  # /app/services/menus/delete_menu_service.rb
  class DeleteMenuService
    include Service

    def initialize(attrs = {})
      @menu_item = attrs.fetch(:menu)
    end

    def call
      remove_restaurant_menu_item
    end

    private

    attr_reader :menu_item

    def remove_restaurant_menu_item
      menu_item.destroy
    end
  end
end
