# frozen_string_literal: true

module Menus
  # /app/services/menus/get_menu_service.rb
  class GetMenuService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      menus_collection
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def menus
      Menu.where(restaurant_id: params[:restaurant_id]).load_async.order(order).page(page).per(per_page)
    end

    def menus_collection
      menus.empty? ? (raise Error::ResourceNotFound) : menus
    end

    def order
      { sort_by => sort_order }
    end
  end
end
