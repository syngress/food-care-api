# frozen_string_literal: true

module Menus
  # /app/services/menus/update_menu_service.rb
  class UpdateMenuService
    include Service

    attr_reader :form, :menu

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @menu = attrs.fetch(:menu)
    end

    def call
      return build_result(validation_error) unless form.valid?

      menu.update(form.to_hash.compact)
      build_result(menu)
    end
  end
end
