# frozen_string_literal: true

module Menus
  # /app/services/menus/create_menu_service.rb
  class CreateMenuService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @restaurant = attrs.fetch(:restaurant)
    end

    def call
      return build_result(validation_error) unless form.valid?

      build_result(menu)
    end

    private

    attr_reader :form, :restaurant

    def menu
      restaurant.menus.create(form.to_hash)
    end
  end
end
