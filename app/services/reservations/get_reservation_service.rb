# frozen_string_literal: true

module Reservations
  # /app/services/reservations/get_reservation_service.rb
  class GetReservationService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
      @user = attrs.fetch(:user)
    end

    def call
      reservations
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params, :user

    def reservations
      reservations_collection.empty? ? (raise Error::ResourceNotFound) : reservations_collection
    end

    def reservations_collection
      @reservations_collection ||= user.reservations.where(restaurant_id: params[:restaurant_id]).load_async.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
