# frozen_string_literal: true

module Reservations
  # /app/services/reservations/create_reservation_service.rb
  class CreateReservationService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @restaurant = attrs.fetch(:restaurant)
    end

    def call
      return build_result(validation_error) unless form.valid?

      Booking::BookingWorker.perform_at(1.day.from_now.beginning_of_day + 13.hours, form.email, restaurant.name)
      build_result(reservation)
    end

    private

    attr_reader :form, :restaurant

    def reservation
      @reservation ||= restaurant.reservations.create(form.to_hash)
    end
  end
end
