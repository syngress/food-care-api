# frozen_string_literal: true

module Reservations
  # /app/services/reservations/update_reservation_service.rb
  class UpdateReservationService
    include Service

    attr_reader :form, :reservation

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @reservation = attrs.fetch(:reservation)
    end

    def call
      return build_result(validation_error) unless form.valid?

      reservation.update(form.to_hash.compact)
      build_result(reservation)
    end
  end
end
