# frozen_string_literal: true

module Reservations
  # /app/services/reservations/delete_reservation_service.rb
  class DeleteReservationService
    include Service

    def initialize(attrs = {})
      @reservation = attrs.fetch(:reservation)
    end

    def call
      remove_user_reservation
    end

    private

    attr_reader :reservation

    def remove_user_reservation
      reservation.destroy
    end
  end
end
