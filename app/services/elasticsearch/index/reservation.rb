# frozen_string_literal: true

module Elasticsearch
  module Index
    # app/services/elasticsearch/index/reservation.rb
    class Reservation
      include Service

      def initialize(attrs = {})
        @method = attrs.fetch(:method)
        @record = attrs.fetch(:record)
        @restaurant = attrs.fetch(:restaurant, nil)
      end

      def call
        return unless Settings.elasticsearch.enabled?

        if Settings.async
          Elasticsearch::Index::ReservationWorker.perform_async({ method: method, record_id: record.id }.to_json)
        else
          Elasticsearch::Index::ManifestDocument.call(method: method, record: record)
        end
      end

      private

      attr_reader :method, :record, :restaurant
    end
  end
end
