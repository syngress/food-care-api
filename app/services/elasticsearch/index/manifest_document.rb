# frozen_string_literal: true

module Elasticsearch
  module Index
    # app/services/elasticsearch/index/manifest_document.rb
    class ManifestDocument
      include Service

      def initialize(attrs = {})
        @record = attrs.fetch(:record).__elasticsearch__
        @method = attrs.fetch(:method)
      end

      def call
        case method.to_sym
        when :create
          record.index_document(options) unless record.client.exists(options)
        when :destroy
          record.delete_document(options)
        when :update
          if record.client.exists(options)
            record.update_document_attributes(record.as_indexed_json, options)
          else
            record.index_document(options)
          end
        end
      end

      private

      attr_reader :record, :method

      def options
        @options ||= {
          index: "#{Settings.elasticsearch.index_name}-#{record.created_at.strftime('%Y.%m')}",
          type: record.document_type,
          id: record.id
        }
      end
    end
  end
end
