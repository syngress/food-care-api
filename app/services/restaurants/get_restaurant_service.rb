# frozen_string_literal: true

module Restaurants
  # /app/services/restaurants/get_restaurant_service.rb
  class GetRestaurantService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
    end

    def call
      restaurants_data
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page

    def restaurants_data
      restaurants_collection.order(order).page(page).per(per_page)
    end

    def restaurants_collection
      Restaurant.all.includes(:menus).load_async
    end

    def order
      { sort_by => sort_order }
    end
  end
end
