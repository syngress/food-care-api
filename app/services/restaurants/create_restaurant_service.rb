# frozen_string_literal: true

module Restaurants
  # /app/services/restaurants/create_restaurant_service.rb
  class CreateRestaurantService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @user = attrs.fetch(:user)
    end

    def call
      return build_result(validation_error) unless form.valid?

      build_result(restaurnat)
    end

    private

    attr_reader :form, :user

    def restaurnat
      @restaurnat ||= user.restaurants.create(form.to_hash)
    end
  end
end
