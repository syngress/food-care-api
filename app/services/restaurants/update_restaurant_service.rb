# frozen_string_literal: true

module Restaurants
  # /app/services/restaurants/update_restaurant_service.rb
  class UpdateRestaurantService
    include Service

    attr_reader :form, :restaurant

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @restaurant = attrs.fetch(:restaurant)
    end

    def call
      return build_result(validation_error) unless form.valid?

      restaurant.update(form.to_hash)
      build_result(restaurant)
    end
  end
end
