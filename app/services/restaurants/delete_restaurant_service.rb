# frozen_string_literal: true

module Restaurants
  # /app/services/restaurants/delete_restaurant_service.rb
  class DeleteRestaurantService
    include Service

    def initialize(attrs = {})
      @restaurant = attrs.fetch(:restaurant)
      @user = attrs.fetch(:user)
    end

    def call
      check_restaurant_reservations
    end

    private

    attr_reader :restaurant, :user

    def check_restaurant_reservations
      raise Error::InvalidAction.new(details: "Restaurant #{restaurant.name} hase active reservations") if restaurant.reservations.present?

      restaurant.destroy
    end
  end
end
