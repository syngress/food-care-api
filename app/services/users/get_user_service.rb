# frozen_string_literal: true

module Users
  # /app/services/users/get_user_service.rb
  class GetUserService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
      @user = attrs.fetch(:user)
    end

    def call
      users_collection
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :user

    def users_collection
      user.admin? ? User.all.load_async.order(order).page(page).per(per_page) : [user]
    end

    def order
      { sort_by => sort_order }
    end
  end
end
