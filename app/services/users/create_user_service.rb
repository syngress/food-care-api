# frozen_string_literal: true

module Users
  # /app/services/users/create_user_service.rb
  class CreateUserService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      # All new users generate with regular role
      @id_role = 1
    end

    def call
      return build_result(validation_error) unless form.valid?

      build_result(user)
    end

    private

    attr_reader :form

    def user
      User.create(form.to_h.merge!(id_role: @id_role))
    end
  end
end
