# frozen_string_literal: true

module Users
  # /app/services/users/update_user_service.rb
  class UpdateUserService
    include Service

    attr_reader :form, :user

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @user = attrs.fetch(:user)
    end

    def call
      return build_result(validation_error) unless form.valid?

      user.update(form.to_hash)
      build_result(user)
    end
  end
end
