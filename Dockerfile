FROM phusion/baseimage:focal-1.2.0

# Command "docker run" can override this variables
ENV TZ=Europe/Warsaw

# Set correct environment variables.
ENV HOME /app

RUN cat /etc/os-release

##########################
###### INSTALL LIBS ######
##########################

RUN mkdir -p /app
RUN apt update -y
RUN apt-cache policy libssl1.0-dev
RUN apt-get update && apt-get install -y wget curl build-essential libpq-dev git libsnappy-dev htop zlib1g-dev tzdata iputils-ping openssl libssl-dev
RUN wget "http://ftp.se.debian.org/debian/pool/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u8_amd64.deb"
RUN wget "http://archive.ubuntu.com/ubuntu/pool/main/g/glibc/multiarch-support_2.27-3ubuntu1.6_amd64.deb"
RUN /bin/sh -c 'set -ex && \
    ARCH=`uname -m` && \
    if [ "$ARCH" == "x86_64" ]; then \
       dpkg -i libssl1.0.0_1.0.1t-1+deb8u8_amd64.deb && dpkg -i multiarch-support_2.27-3ubuntu1.6_amd64.deb; \
    else \
       wget http://launchpadlibrarian.net/600853597/multiarch-support_2.27-3ubuntu1.6_arm64.deb && \
       wget http://ports.ubuntu.com/pool/main/o/openssl1.0/libssl1.0.0_1.0.2n-1ubuntu5_arm64.deb && \
       dpkg -i libssl1.0.0_1.0.2n-1ubuntu5_arm64.deb && dpkg -i multiarch-support_2.27-3ubuntu1.6_arm64.deb; \
    fi'
RUN apt-get install -y --no-install-recommends
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt full-upgrade -y

###########################
###### INSTALL NGINX ######
###########################

RUN add-apt-repository ppa:nginx/stable
RUN apt update
RUN apt install -y nginx

# Get nginx signing key
RUN wget http://nginx.org/packages/keys/nginx_signing.key
RUN cat nginx_signing.key | apt-key add -

# Create nginx user
RUN adduser --system --no-create-home --shell /bin/false --group --disabled-login nginx

# Create nginx cache directory
RUN mkdir -p /var/cache/nginx/tmp

# Add the nginx config
ADD docker/config/nginx.conf /etc/nginx/nginx.conf

# Add ssl certs
RUN mkdir -p /etc/nginx/certs
COPY docker/config/certs /etc/nginx/certs

##################################
######## INSTALL RUBY 3.1.2 ######
##################################

#RUN apt-add-repository ppa:brightbox/ruby-ng && apt-get update && apt-get install -y ruby3.1 ruby3.1-dev && gem install bundler
#RUN apt-get install -y nodejs && apt-get clean && rm -rf /var/lib/apt/lists/*

ENV HOME /home/app
ENV PATH $HOME/.rbenv/shims:$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:$PATH
# clone rbenv
RUN git clone https://github.com/rbenv/rbenv.git ~/.rbenv
# clone ruby-build
RUN git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
RUN rbenv install 3.1.2 && rbenv global 3.1.2

# This sets the context of where commands will be ran in and is documented on Docker's website extensively.
WORKDIR /app

# Create pids directory for Puma
RUN mkdir /app/tmp
RUN touch /app/tmp/puma.pid
RUN touch /app/tmp/puma.state

###############################
###### ADD INIT SERVICES ######
###############################

RUN mkdir /etc/service/puma
COPY docker/config/init/puma.sh /etc/service/puma/run
RUN chmod +x /etc/service/puma/run

RUN mkdir /etc/service/nginx
COPY docker/config/init/nginx.sh /etc/service/nginx/run
RUN chmod +x /etc/service/nginx/run

RUN mkdir /etc/service/sidekiq
COPY docker/config/init/sidekiq.sh /etc/service/sidekiq/run
RUN chmod +x /etc/service/sidekiq/run

RUN mkdir /etc/service/sidekiq1
COPY docker/config/init/sidekiq_kafka.sh /etc/service/sidekiq1/run
RUN chmod +x /etc/service/sidekiq1/run

RUN mkdir /etc/service/sidekiq2
COPY docker/config/init/sidekiq_batches.sh /etc/service/sidekiq2/run
RUN chmod +x /etc/service/sidekiq2/run

#################################
###### PREPARE APPLICATION ######
#################################
# Add the Rails app
ADD . /app

# Ensure gems are cached and only get updated when they change.
# This will drastically increase build times when your gems do not change.
COPY Gemfile Gemfile
RUN gem install bundler && bundle install

# EXPOSE 8080 for http connections
EXPOSE 443
