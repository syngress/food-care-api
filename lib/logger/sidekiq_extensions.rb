# frozen_string_literal: true

# lib/logger/sidekiq_extensions.rb
class ExceptionLogger
  def self.call(_exc, ctx_hash)
    items = ctx_hash[:job].symbolize_keys
    Sidekiq.logger.error do
      items.slice(:class, :queue, :args, :jid).merge!(
        exception: { class: items[:error_class], message: items[:error_message], backtrace: items[:error_backtrace] },
        duration: nil
      )
    end
  end
end

# lib/logger/sidekiq_extensions.rb
class JobLogger
  def call(items, _queue)
    items = items.symbolize_keys
    start = Time.now
    yield
    Sidekiq.logger.info { items.slice(:class, :queue, :args, :jid).merge!(duration: (Time.now - start).round(3)) }
  end
end
