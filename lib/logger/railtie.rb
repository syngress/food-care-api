# frozen_string_literal: true

require_relative 'device/file'
require_relative 'device/logstash'
require_relative 'custom_logger'
require_relative 'sidekiq_extensions'
require_relative 'request_log_subscriber'

# lib/logger/railtie.rb
class Railtie < Rails::Railtie
  initializer 'disable_logger_middleware', after: true do |app|
    app.middleware.delete Rails::Rack::Logger if Settings.custom_logger.enabled
  end

  config.after_initialize do
    Rails.module_eval do
      def self.event_logger
        @event_logger ||= CustomLogger.new(log_type: :log)
      end
    end

    if Settings.custom_logger.enabled
      ActiveSupport::LogSubscriber.log_subscribers.each do |subscriber|
        if subscriber.class.to_s == 'ActionController::LogSubscriber'
          unsubscribe(:action_controller, subscriber)
        end
      end

      RequestLogSubscriber.attach_to :action_controller
    end
  end

  def unsubscribe(component, subscriber)
    events = subscriber.public_methods(false).reject { |method| method.to_s == 'call' }
    events.each do |event|
      ActiveSupport::Notifications.notifier.listeners_for("#{event}.#{component}").each do |listener|
        if listener.instance_variable_get('@delegate') == subscriber
          ActiveSupport::Notifications.unsubscribe listener
        end
      end
    end
  end
end
