# frozen_string_literal: true

# lib/logger/request_log_subscriber.rb
class RequestLogSubscriber < ActiveSupport::LogSubscriber
  INTERNAL_PARAMS = %w[controller action format _method only_path].freeze

  def process_action(event)
    return if event.payload[:controller] == MiddlewareErrorsController.name

    data = extract_request(event, event.payload)
    logger.send(severity(event), data)
  end

  def redirect_to(event)
    Thread.current[:redirect_to] = event.payload[:location]
  end

  def unpermitted_parameters(event)
    Thread.current[:unpermitted_params] ||= []
    Thread.current[:unpermitted_params].concat(event.payload[:keys])
  end

  private

  def severity(event)
    case event.payload[:status].to_s
    when /5\d{2}/ then :error
    when /4\d{2}/ then :warn
    else :info
    end
  end

  def extract_request(event, payload)
    payload ||= event.payload
    data = initial_data(payload)
    data.merge!(extract_status(payload))
    data.merge!(extract_runtimes(event, payload))
    data.merge!(extract_location)
    data.merge!(extract_unpermitted_params)
  end

  def initial_data(payload)
    {
      method: payload[:method],
      path: extract_path(payload),
      format: payload[:format],
      controller: payload[:controller],
      action: payload[:action],
      params: payload[:params].except(*INTERNAL_PARAMS),
      exception: Thread.current[:exception]
    }.compact
  end

  def extract_path(payload)
    path = payload[:path]
    index = path.index('?')
    index ? path[0, index] : path
  end

  def extract_status(payload)
    if (status = payload[:status])
      { status: status.to_i }
    elsif (error = payload[:exception])
      exception, message = error
      { status: get_error_status_code(exception), error: "#{exception}: #{message}" }
    else
      { status: 0 }
    end
  end

  def get_error_status_code(exception)
    status = ActionDispatch::ExceptionWrapper.rescue_responses[exception]
    Rack::Utils.status_code(status)
  end

  def extract_runtimes(event, payload)
    data = { duration: event.duration.to_f.round(2) }
    data.merge!(view: payload[:view_runtime].to_f.round(2)) if payload.key?(:view_runtime)
    data.merge!(db: payload[:db_runtime].to_f.round(2)) if payload.key?(:db_runtime)
    data.merge!(es: payload[:elasticsearch_runtime].to_f.round(2)) if payload.key?(:elasticsearch_runtime)
    data
  end

  def extract_location
    location = Thread.current[:redirect_to]
    return {} unless location

    Thread.current[:redirect_to] = nil
    { location: location }
  end

  def extract_unpermitted_params
    unpermitted_params = Thread.current[:unpermitted_params]
    return {} unless unpermitted_params

    Thread.current[:unpermitted_params] = nil
    { unpermitted_params: unpermitted_params }
  end
end
