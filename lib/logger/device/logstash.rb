# frozen_string_literal: true

module Device
  # lib/logger/device/logstash.rb
  class Logstash
    def close; end

    def open(*); end

    def write(message)
      Kafka::Responders::LogstashResponder.call(message)
    rescue Kafka::DeliveryFailed => e
      raise e
    end
  end
end
