# frozen_string_literal: true

require 'fileutils'

module Device
  # lib/logger/device/file.rb
  class File
    def write(message)
      open
      @io.write(message)
      @io.write('\n' * 2)
      close
    end

    def close
      @io.close if @io.present?
    end

    def open
      unless ::File.exist?(::File.dirname(path))
        ::FileUtils.mkdir_p(::File.dirname(path))
      end
      @io = ::File.open(path, ::File::WRONLY | ::File::APPEND | ::File::CREAT)
      @io.binmode
    end

    def path
      @path ||= Settings.custom_logger.file_path.presence || Rails.root.join("log/#{Rails.env}.log")
    end
  end
end
