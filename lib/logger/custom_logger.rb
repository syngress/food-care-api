# frozen_string_literal: true

# lib/logger/custom_logger.rb
module CustomLogger
  class << self
    def new(attrs = {})
      ::Logger.new(device).tap do |logger|
        logger.instance_variable_set(:@device, device)
        logger.instance_variable_set(:@log_type, attrs.fetch(:log_type, :trace))
        logger.extend(self)
      end
    end

    def device
      @device ||= (Settings.custom_logger.type == 'file' ? Device::File : Device::Logstash).new
    end
  end

  attr_reader :log_type

  def add(severity, message = nil, progname = nil, &block)
    super if (progname.present? && progname.is_a?(Hash)) || (block_given? && yield.is_a?(Hash))
  end

  def format_message(severity, timestamp, _progname, message)
    {
      severity: severity,
      log_type: log_type,
      app_version: 'some version',
      country: Settings.country,
      fqdn: Settings.custom_logger.fqdn,
      created_at: timestamp.strftime('%FT%T%:z'),
      request_ip: Thread.current[:request_ip],
      iid: Thread.current[:request_id],
      uuid: SecureRandom.uuid,
      method: nil,
      path: nil,
      action: nil,
      status: nil,
      duration: nil,
      view: nil,
      db: nil,
      es: nil,
      details: message.to_json
    }.to_json
  end
end
