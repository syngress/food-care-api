# frozen_string_literal: true

workers ENV.fetch('WEB_CONCURRENCY', 2)
threads 2, ENV.fetch('RAILS_MAX_THREADS', 16)

# preload_app!

rackup DefaultRackup
environment ENV.fetch('RAILS_ENV', 'development')
pidfile 'tmp/puma.pid'
state_path 'tmp/puma.state'
bind 'tcp://0.0.0.0:9292'
# bind 'unix:///var/run/my_app.sock'

# daemonize false

on_worker_boot do
  ActiveRecord::Base.establish_connection if defined?(ActiveRecord)
end

on_restart do
  Sidekiq.redis.shutdown(&:close)
end

plugin :tmp_restart
