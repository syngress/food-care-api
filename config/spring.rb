# frozen_string_literal: true

%w[
  .ruby-version
  .rbenv-vars
  tmp/restart.txt
  tmp/caching-dev.txt
  tmp/puma.pid
  tmp/puma.state
].each { |path| Spring.watch(path) }
