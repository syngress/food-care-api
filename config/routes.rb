# frozen_string_literal: true

Rails.application.routes.draw do
  Sidekiq::Web.use(::Rack::Protection, { use: :authenticity_token, logging: true, message: "Didn't work that way lad!" })
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Settings.basic_auth.username && password == Settings.basic_auth.password
  end
  mount Sidekiq::Web => '/sidekiq'
  resources :restaurants, only: %i[index show create update destroy] do
    collection do
      get :restaurants_collection
    end
    resources :reservations, only: %i[index show create update destroy], shallow: true
    resources :menus, only: %i[index show create update destroy], shallow: true
    resources :tables, only: %i[index show create update destroy], shallow: true
  end
  resources :users, param: :id
  post '/auth/login', to: 'authentication#login'
  get '/*a', to: 'application#not_found'
end
