# frozen_string_literal: true

require 'avro_turf/messaging'

avro_schema_store = AvroTurf::SchemaStore.new(path: "#{Rails.root}/db/avro_schemas")
avro_schema_registry = AvroTurf::CachedConfluentSchemaRegistry.new(
  AvroTurf::ConfluentSchemaRegistry.new(Settings.kafka.avro_schema_registry_url)
)
$avro = AvroTurf::Messaging.new(schema_store: avro_schema_store, registry: avro_schema_registry)
