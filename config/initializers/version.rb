# frozen_string_literal: true

# config/initializers/version.rb
module FoodcareAPI
  ORIGIN_NAME = 'foodcare'
  VERSION = '1.0.1'

  def self.info
    { version: VERSION }
  end
end
