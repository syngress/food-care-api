# frozen_string_literal: true

require 'middleware/sidekiq_client'
require 'middleware/sidekiq_server'
require 'sidekiq/web'
require 'sidekiq/cron/web'

Sidekiq.configure_client do |config|
  config.redis = REDIS_SETTINGS
  config.client_middleware do |chain|
    chain.add Middleware::SidekiqClient
  end
end

Sidekiq.configure_server do |config|
  config.redis = REDIS_SETTINGS
  config.client_middleware do |chain|
    chain.add Middleware::SidekiqClient
  end
  config.server_middleware do |chain|
    chain.add Middleware::SidekiqServer
  end
end

if Settings.custom_logger.sidekiq_enabled?
  Sidekiq.logger = CustomLogger.new
  Sidekiq.logger.level = Settings.custom_logger.sidekiq_log_level
  Sidekiq.options[:job_logger] = JobLogger
  Sidekiq.options[:error_handlers] = [ExceptionLogger]
elsif Rails.env.production?
  Sidekiq.logger = nil
end
