# frozen_string_literal: true

require_relative 'boot'

require 'rails'
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'rails/test_unit/railtie'
require 'zip'
require 'action_cable/engine'
require 'elasticsearch/model'
require 'elasticsearch/rails/instrumentation'
require_relative '../lib/logger/railtie'
require_relative '../lib/middleware/setup_request_id'
require_relative '../lib/middleware/catch_json_parse_errors'
require_relative '../lib/middleware/set_locale'

Bundler.require(*Rails.groups)

module Foodcare
  # config/application.rb
  class Application < Rails::Application
    config.i18n.available_locales = %i[en en_GB pl_PL]
    config.i18n.default_locale = Settings.i18n.default_locale
    config.i18n.enforce_available_locales = false
    config.i18n.fallbacks = Settings.i18n.fallbacks
    config.time_zone = :local
    config.active_record.default_timezone = :local

    if Settings.custom_logger.enabled
      config.colorize_logging = true
      config.logger = CustomLogger.new
      config.log_level = Settings.custom_logger.log_level
    end

    config.exceptions_app = lambda do |env|
      MiddlewareErrorsController.action(:show).call(env)
    end

    config.time_zone = 'Warsaw'

    config.api_only = true
    config.middleware.insert_after Rack::Head, Middleware::SetupRequestId
    config.middleware.insert_before Middleware::SetupRequestId, Middleware::CatchJsonParseErrors
    config.middleware.insert_before Middleware::CatchJsonParseErrors, Middleware::SetLocale
    config.middleware.use ActionDispatch::Session::CookieStore, key: '_foodcare_session'
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post delete put patch options head]
      end
    end
  end
end
