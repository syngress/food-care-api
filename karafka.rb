# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'development'
ENV['KARAFKA_ENV'] = ENV.fetch('RAILS_ENV')
require ::File.expand_path('config/environment', __dir__)
Rails.application.eager_load!
# karafka.rb
module ConsumerMapper
  def self.call(consumer_group_name)
    consumer_group_name
  end
end

# karafka.rb
class KarafkaApp < Karafka::App
  setup do |config|
    config.kafka.seed_brokers = Settings.kafka.seed_brokers
    config.kafka.connect_timeout = Settings.kafka.connect_timeout
    config.kafka.socket_timeout = Settings.kafka.socket_timeout
    config.kafka.heartbeat_interval = Settings.kafka.heartbeat_interval
    config.kafka.session_timeout = Settings.kafka.session_timeout
    config.kafka.pause_timeout = Settings.kafka.pause_timeout
    config.kafka.fetcher_max_queue_size = Settings.kafka.fetcher_max_queue_size
    config.kafka.offset_commit_interval = Settings.kafka.offset_commit_interval
    config.kafka.offset_commit_threshold = Settings.kafka.offset_commit_threshold
    config.client_id = 'kafka'
    config.backend = :inline
    config.batch_fetching = true
    config.batch_consuming = true
    config.consumer_mapper = ConsumerMapper
  end
end

Karafka.monitor.subscribe('app.initialized') do
  # Put here all the things you want to do after the Karafka framework initialization
end

KarafkaApp.boot!
