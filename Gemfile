# frozen_string_literal: true

source 'https://rubygems.org'

# Avro schema
gem 'avro_turf', '1.5.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.18'
# Configuration
gem 'config', '4.0.0'
# Virtus hash to array
gem 'coercible-hash_to_array', '0.0.1'
# Validation
gem 'dry-schema', '1.9.3'
# Search
gem 'elasticsearch-model', '~> 7.2'
gem 'elasticsearch-rails', '~> 7.2'
# Serialize data by Netflix
gem 'fast_jsonapi', '~> 1.5'
# JSON WebToken
gem 'jwt', '~> 2.3.0'
# Pagination
gem 'kaminari', '1.2.2'
# Kafka pipeline
gem 'karafka', '1.4.13'
# Logs
gem 'lograge', '0.12.0'
# Postgres
gem 'pg', '~> 1.4.1'
# Use Puma as the app server
gem 'puma', '5.6.4'
# Rack-Corse
gem 'rack-cors', '1.1.1'
# Rack Protection
gem 'rack-protection', '2.2.0'
# Rails framework
gem 'rails', '~> 7.0.3'
# Redis
gem 'redis-rails', '5.0.2'
# Compression
gem 'rubyzip', '2.3.2'
# Clean Code
gem 'rubocop', '1.30.0'
# Sidekiq
gem 'sidekiq', '6.4.2'
gem 'sidekiq-batch', '0.1.8'
gem 'sidekiq-cron', '1.4.0'
gem 'sidekiq-monitor-stats', '0.0.4'
# Compression
gem 'snappy', '0.3.0'
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', '1.2022.1'
# Forms
gem 'virtus', '2.0.0'

group :development, :test do
  gem 'database_cleaner', '~> 2.0'
  gem 'factory_bot_rails', '~> 6.2'
  gem 'faker', '~> 2.21'
  gem 'pry', '0.14.1'
  gem 'pry-nav', '1.0.0'
  gem 'rspec-its', '~> 1.3'
  gem 'rspec-rails', '~> 5.1'
  gem 'shoulda-matchers', '~> 5.1'
  gem 'simplecov', '~> 0.21'
  gem 'webmock', '~> 3.14'
end

group :development do
  gem 'listen', '~> 3.7'
end
