#!/bin/sh
if [ "$SIDEKIQ_DISABLED" != "1" ] ; then
  sh -c 'cd /app && bundle exec sidekiq -q elasticsearch -t 20 -c $EXTRA_SIDEKIQ_THREADS -e $RAILS_ENV > /dev/null'
fi
