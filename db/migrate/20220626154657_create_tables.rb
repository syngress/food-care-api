# frozen_string_literal: true

# db/migrate/20220626154657_create_tables.rb
class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.string :name
      t.integer :capacity
      t.references :restaurant, null: false, foreign_key: true

      t.timestamps
    end
  end
end
