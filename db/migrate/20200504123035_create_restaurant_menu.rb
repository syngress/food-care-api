# frozen_string_literal: true

# db/migrate/20200504123035_create_restaurant_menu.rb
class CreateRestaurantMenu < ActiveRecord::Migration[5.2]
  def change
    create_table :menus do |t|
      t.string :category
      t.string :name
      t.string :description
      t.decimal :price
      t.string :currency
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
