# frozen_string_literal: true

# db/migrate/20180630194231_create_reservations.rb
class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.integer :guest_number
      t.integer :table_id
      t.integer :extend_time
      t.references :restaurant, foreign_key: true

      t.timestamps
    end

    add_index :reservations, :email
    add_index :reservations, :table_id
  end
end
