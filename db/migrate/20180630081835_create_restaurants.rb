# frozen_string_literal: true

# db/migrate/20180630081835_create_restaurants.rb
class CreateRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :address
      t.string :description
      t.integer :guest_capacity
      t.integer :booking_capacity
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :restaurants, :name, unique: true
  end
end
