# frozen_string_literal: true

# db/migrate/20180317112855_create_users.rb
class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :username
      t.string :email
      t.string :password_digest
      t.integer :id_role

      t.timestamps
    end
  end
end
