# frozen_string_literal: true

require 'faker'
require 'pry'
require 'active_record'
require 'benchmark'
require 'bcrypt'

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  database: 'foodcare_development',
  encoding: 'unicode',
  host: '192.168.0.55',
  port: '29196',
  username: 'postgres',
  password: 'postgres',
  pool: 25
)
ActiveRecord::Base.connection.execute('')

module FakeData
  class Table < ActiveRecord::Base
  end
end

module FakeData
  class Menu < ActiveRecord::Base
  end
end

module FakeData
  # Restaurant
  class Restaurant < ActiveRecord::Base
    has_many :tables, foreign_key: :restaurant_id
    has_many :menus, foreign_key: :restaurant_id

    def call
      restaurants = Restaurant.all.load_async
      create_restaurant_menus(restaurants)
      create_restaurant_tables(restaurants)
    end

    def create_restaurant_menus(restaurants)
      Benchmark.bm(25) do |bm|
        bm.report('Create Menus Time:') do
          restaurants.each do |restaurant|
            10.times do
              restaurant.menus.create!(
                category: 'default',
                name: "#{Faker::Food.dish} with #{Faker::Food.spice}",
                description: Faker::Food.description,
                price: Faker::Number.decimal(l_digits: 2),
                currency: 'PLN'
              )
            end
          end
        end
      end
    end

    def create_restaurant_tables(restaurants)
      i = 0
      Benchmark.bm(25) do |bm|
        bm.report('Create Tables Time:') do
          restaurants.each do |restaurant|
            10.times do
              i += 1
              restaurant.tables.create!(
                name: "table_#{i}",
                capacity: rand(2..10)
              )
            end
          end
        end
      end
    end
  end
end

module FakeData
  # User
  class User < ActiveRecord::Base
    has_secure_password
    has_many :restaurants, foreign_key: :user_id, class_name: 'FakeData::Restaurant'

    def call
      create_users
      create_restaurants
    end

    def create_users
      string_length = 8
      Benchmark.bm(25) do |bm|
        bm.report('Create Users Time:') do
          100.times do
            User.create!(
              name: Faker::Name.male_first_name,
              username: rand(36**string_length).to_s(36),
              email: Faker::Internet.email,
              password_digest: nil,
              id_role: 1,
              password: 'admin',
              created_at: Time.now,
              updated_at: Time.now
            )
          end
        end
      end
    end

    def create_restaurants
      Benchmark.bm(25) do |bm|
        bm.report('Create Restaurants Time:') do
          users = User.all.load_async
          users.each do |user|
            10.times do
              user.restaurants.create!(
                name: Faker::Restaurant.unique.name,
                address: Faker::Address.full_address,
                description: Faker::Restaurant.description,
                guest_capacity: rand(1..10),
                booking_capacity: rand(1..10),
                created_at: Time.now,
                updated_at: Time.now
              )
            end
          end
        end
      end
    end
  end
end

FakeData::User.new.call
FakeData::Restaurant.new.call
