# FoodcareSystem
![RubyOnRailsIMG](https://logo.clearbit.com/rubyonrails.org)

Things you may want to cover for local development:

* Ruby  
  `3.1.2`

* Rails  
  `7.0.3`  

* System dependencies and Services (job queues, cache servers, search engines, etc.)  
  `Redis 5.0.3`  
  `Postgres 1.1.4`  
  `Puma 3.12.0`  
  `Docker 20.10.8`  
  `docker-compose 1.29.2`  

* Kafka Stack (latest, use confluentinc distro)  
  `Apache Kafka`  
  `Schema-Registry`  
  `Zookeeper`  
  `Apache Avro`  

* Configuration  
  `Setup SMTP in config/email.yml for email notifications`  
  `Setup path for postgres data volume in docker-compose.yml`    
  `Running from Linux ? Add ` *vm.max_map_count=262144* ` to your host system ` */etc/sysctl.d/99-sysctl.conf* ` file`  
  `Running from localhost ? Add ` *127.0.0.1 kafka* `to` */etc/hosts*  

* Database creation and initialization, running from docker-compose ? sh to container.  
  `rake db:create`  
  `rake db:migrate`  

* Requirements  
  `JSON API`  
  `Use Netflix Fast Jsonapi Serializers`  
  `For OSX install snappy lib from brew`  
  `For Linux install libsnappy-dev`  
  `Application uses its own self signed certificates (docker/certs) change it to your own certs`  

# Data Seed  

You can always seed your database with fake data generator script.  
From main application folder just hit `ruby db/fake_data/fake_data_generator.rb`.  
Remember to change your database configuration inside fake data generator file.  
Default password for users: `admin`  

# Docker  

When you run application from Docker, give stack few minutes allowing communicate and configure between docker components.  
Remember that stack includes services such as Kafka and Elasticsearch, it takes time to get all components talk together.  
After few minutes, hit `docker ps` and check if all containers are working.  

# Sidekiq Batches  

Now you don't need to use *Sidekiq-PRO* to work with Batches ;)  
With little workaround you can implement batches to catch success batch bid's..  
Implemented mechanism creates a batch and sends an async task.  
If task is successful, a message is sent to kafka topic.  

# Micro restaurant network  

As a restaurant network owner I need a system which will allow me to add a restaurant to my database and see which restaurants I can manage.

 **Create a restaurant**  
 **See a list of restaurants**  
 **Paginated results**  
 **Sortable results**  

## Booking system  

As restaurant owner, I want to add a booking service to all of my restaurants

* Create booking service for all customers  
* Allow to create, update, destroy restaurant for authorized requests
* Allow everyone to get restaurant item or restaurant collection, return reservations and tables with restaurant response  
* Allow to create reservation for unauthorized requests
* Allow to get reservation or reservation collection for authorized requests  
* Restaurant owner can see only own restaurants and reservations    
* In a restaurants list, return number of reservations  
* Restaurant has tables that have a predefined number of seats  
* Allow to create restaurant tables with table names and seating capacity  
* Allow to get restaurant table collection for authorized requests  
* Allow everyone to get restaurant table  
* Allow to create, update, destroy restaurant menu for authorized requests  
* Allow everyone to get menu item or menu collection  
* Paginated results  
* Sortable results  

## Limitations

Each of the restaurants can hold maximum of *X* people or *Y* bookings, restaurants work from *X AM* to *Y PM* (setup from configuration)

* Set a validation when creating a booking
* There may be maximum XX bookings for restaurant per 1 day, setup limit from database.
* There may be maximum YY people booked per restaurant per 1 day, setup limit from databse.
* Can't create reservation, if date-time is between (6 PM - 9 PM) time window, throw error through validation form:  
##
```code
{
    "status": 400,
    "error": "validation_failed",
    "message": "Validation Failed",
    "details": {
        "reservation": [
            "We apologize, between 18:00 and 21:00 our reservation system has been suspended"
        ]
    }
}
```

* Default reservation time is 120 minutes, setup from configuration  
* Allow client to extend default reservation time, when a booking order takes place  
* Allow owner to update time of an ongoing reservation, limit to 8 hours max !  
* Prevent new reservations with a reserved table, display appropriate message  

## Reminders

I want to send my customers a `see you next time` email next day.

* At 1 PM next day of booking, send email to customer  
* Use Sidekiq queue  
* Automatically send same encoded message to Kafka pipeline using apache Avro schema  
* Receive message from pipeline through consumer written in any language  

# Elasticsearch

- Dynamically create one `Index Alias` with a separate new `Index` with each new day.  
	> Save data related to reservations

# Application Containerization

- Application should be containerized using `phusion baseimage` Ubuntu system, along with Postgres and Redis services
- Create docker image for the rails foodcareapi application
- Create container for the PostgreSQL database and connect this service with your app container
- Postgres data should remain on local hard drive in the desired location
- Create container for Redis server and connect this service with your app container
- Use latest phusion/baseimage with separate source configuration
- Use Nginx and configure it as proccess (not service)

# Custom Logger

- Create custom logging functionality with 2 sources `*file, easticsearch*`  
- Allow to setup log level from application configuration
- Create path to file from configuration and send logs to that file
- Create some log structure for file and Elasticsearch
- Logging in to elastic automatically turns off file logging
- Let filebeat read the application logs and send them to logstash
- Let logstash automatically send data to elasticsearch

# Kafka Stack

- Create Apache Kafka container from Confluentinc distro and expose it on port 9092
- Create Zookeeper container from Confluentinc distro and connect it with kafka container on port 2181
- Create SchemRegistry container from Confluentinc distro and connect it with zookeeper container on port 2181 and expose it on port 8081
- Create KafkaManager container from Sheepkiller distro and connect it with zookeeper container on port 2181 and expose it on port 9100

# Elasticsearch Stack

- Create 3 container nodes of Elasticsearch Cluster and expose it on port 9200
- Create Kibana container and connect it with Elasticsearch container on port 9200 and expose it on port 5601
- Create Filebeat container and connect it with Logstash container on port 5044
- Allow Logstash to read Kafka Topics and send data to Elasticsearch

# Language (PL/EN)

  - It should be possible to provide the response in the chosen language (messages and errors).
    > Passing in the header (Accept-Language: `pl_PL`) or (Accept-Language: `en_EN`)

# Table of endpoint

USERS  

Users have access to their restaurants after prior authentication, use JWT.  
To receive the resource, you must pass valid token in the request header.  
There are 2 levels of data access (regular / admin)  
Each new user is registered with the regular level (1).  
Administrative level can only be obtained by manually changing the database value in the id_role column (regular = 1 and admin = 2)  
Administrative level provides all restaurants and all reservations, so database should be in a different network segment.  
Passwords are not stored in plain text !! :)  

|Method          |Endpoint                                  |Note                            |
|----------------|------------------------------------------|--------------------------------|
|POST			       |`/users`            	                    |Register New User               |
|POST            |`/auth/login`                             |Authorize User                  |
|GET             |`/users`                                  |Get Users Collection            |
|GET			       |`/users/:id`                              |Get User Object  	             |
|PUT			       |`/users/:id`            	                |Update User Object     	       |

RESTAURANTS

|Method          |Endpoint                                  |Note                             |
|----------------|------------------------------------------|---------------------------------|
|GET			       |`/restaurants`            	            |Get Restaurant Collection          |
|GET             |`/restaurants/:id`                        |Get Restaurant Object            |
|GET             |`/restaurants?sort_by=name&sort_order=asc`|Get Collection Sorted            |
|GET			       |`/restaurants?page=1&per_page=10`         |Get Collection Paged  	          |
|GET			       |`/restaurants/restaurants_collection`     |Get Collection Paged Unauthorized|
|POST			       |`/restaurants`            	            |Create Restaurant Object 	        |
|PUT			       |`/restaurants/:id`            	        |Update Restaurant Object           |

RESERVATIONS

|Method          |Endpoint                                                 |Note                            |
|----------------|---------------------------------------------------------|--------------------------------|
|GET			       |`/restaurants/1/reservations`                            |Get Reservation Collection      |
|GET             |`/reservations/:id`                                      |Get Reservation Object          |
|GET             |`/restaurants/1/reservations?sort_by=name&sort_order=asc`|Get Collection Sorted           |
|GET			       |`/restaurants/1/reservations?page=1&per_page=10`         |Get Collection Paged  	        |
|POST			       |`/restaurants/1/reservations`            	               |Create Reservation Object 	    |
|PUT			       |`/reservations/:id`            	                         |Update Reservation Object     	|

MENUS

|Method          |Endpoint                                           |Note                       |
|----------------|---------------------------------------------------|---------------------------|
|GET			       |`/restaurants/1/menus`                             |Get Menu Collection        |
|GET             |`/menus/:id`                                       |Get Menu Object            |
|GET             |`/restaurants/1/menus?sort_by=name&sort_order=asc` |Get Menu Collection Sorted |
|GET			       |`/restaurants/1/menus?page=1&per_page=10`          |Get Menu Collection Paged  |
|POST			       |`/restaurants/1/menus`            	               |Create Menu Object 	       |
|PUT			       |`/menus/:id`            	                         |Update Menu Object     	   |

TABLES

|Method         |Endpoint                                            |Note                       |
|---------------|----------------------------------------------------|---------------------------|
|GET            |`/restaurants/1/tables?sort_by=name&sort_order=asc` |Get Table Collection Sorted|
|POST           |`/restaurants/1/tables`                             |Create Table Object        |
|GET            |`/tables/:id`                                       |Get Table Object           |
|PUT            |`/tables/:id`                                       |Update Table Object        |
|DELETE         |`/tables/:id`                                       |Delete Table Object        |

# Examples

- Register new user

```sh
{
	"name": "TestUser",
	"username": "mydesiredusername",
	"email": "test@mail.com",
	"password": "Test,111",
	"password_confirmation": "Test,111"
}
```
- Authorize users

```sh
{
	"email": "test@mail.com",
	"password": "Test,111"
}
```

- Create restaurant

```sh
{
	"name": "Restaurant1",
	"address": "31-200, Wolicka 10, Warszaw",
	"description": "Best Exotic Food",
	"guest_capacity": 5,
	"booking_capacity": 7
}
```

- Get restaurants Collection  

```sh
    Header: Authorization: user_token  
    Header: Content-Type:application/json  
```  

- Create new reservation

```sh
{
	"name": "CustomerName",
	"surname": "CustomerSurname",
	"email": "hungry@customer.com",
	"guest_number": 3
}
```

# Dockerize application

- Create and start application containers

> $ docker-compose up

- Build containers from scratch

> $ docker-compose build --no-cache

- Show build images

> $ docker images

- List running containers

> $ docker-compose ps

- Stop all containers

> $ docker stop $(docker ps -a -q)

- Remove all images

> $ docker rmi -f $(docker images -a -q)

- Complete cleanup

> $ docker system prune

# Application Testing

Food Care API application is tested using Rspec framework.  
Tests cover most important functionalities.  
You can check test coverage by viewing test coverage report:  

![TestCoverageReport](https://syngress.pl/images/test_coverage.png)

After each overall application test, report generates index file in this location:  

```
coverage/index.html
```  

# Sequence Diagram For Restaurant Reservation

![ReservationUML](https://syngress.pl/images/foodcare_api/reservation_diagram.png)

**THIS PROJECT IS STILL UNDER CONSTRUCTION**  

Frontend for this API can be found here (react):  
https://bitbucket.org/syngress/food-care-client/src/master/  

Kafka Consumer for this API can be found here (java):  
https://bitbucket.org/syngress/food-care-kafka-consumer/src/master/  
